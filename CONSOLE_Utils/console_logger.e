note
	description: "Utility that handles print requests with custom wording"
	author: "Florent Perreault"
	date: "$Date$"
	revision: "$Revision$"

class
	CONSOLE_LOGGER

inherit
--	SHARED_EXECUTION_ENVIRONMENT	-- might be needed but check for validity and will cause us to rename default_create to something else
	ANY
		redefine
			default_create
		end

create
	default_create

feature	-- INIT

	default_create
			-- Instantiate Current object.
		do
--			create ansi_control.default_create

		end

feature -- LOGGING
	log_warn(msg: STRING_8)
		-- LOGS A WARNING TO THE TERMINAL
	do
		if ansi_control.ansi_enabled then
			ansi_control.set_foreground_color_to_yellow
			print ("WARN: ")
			ansi_control.save_cursor
			ansi_control.set_foreground_color_to_white
			print (msg.out+"%N")
			ansi_control.reset_foreground_color
		else
			print ("WARN: "+msg.out+"%N")
		end
	end

	log_info(msg: STRING_8)
		-- LOGS USEFUL INFO TO THE TERMINAL
	do
		if ansi_control.ansi_enabled then
			ansi_control.set_foreground_color_to_cyan
			print ("INFO: ")
			ansi_control.save_cursor
			ansi_control.set_foreground_color_to_white
			print (msg.out+"%N")
			ansi_control.reset_foreground_color
		else
			print("INFO: "+msg.out+"%N")
		end
	end

	log_fail(msg: STRING_8)
		-- LOGS A FAILED OPERATION TO THE TERMINAL
	do
		if ansi_control.ansi_enabled then
			ansi_control.set_foreground_color_to_magenta
			print ("FAIL: ")
			ansi_control.save_cursor
			ansi_control.set_foreground_color_to_white
			print (msg.out+"%N")
			ansi_control.reset_foreground_color
		else
			print ("FAIL: "+msg.out+"%N")
		end
	end

	log_err(msg: STRING_8)
		-- LOGS A OPERATIONAL ERROR TO THE TERMINAL
	do
		if ansi_control.ansi_enabled then
			ansi_control.set_foreground_color_to_red
			print ("ERROR: ")
			ansi_control.save_cursor
			ansi_control.set_foreground_color_to_white
			print (msg.out+"%N")
			ansi_control.reset_foreground_color
		else
			print ("ERROR: "+msg.out+"%N")
		end
	end

	log_succ(msg: STRING_8)
		-- LOGS A SUCCESSFUL OPERATION TO THE TERMINAL
	do
		if ansi_control.ansi_enabled then
			ansi_control.set_foreground_color_to_green
			print ("SUCCESS: ")
			ansi_control.save_cursor
			ansi_control.set_foreground_color_to_white
			print (msg.out+"%N")
			ansi_control.reset_foreground_color
		else
			print ("SUCCESS: "+msg.out+"%N")
		end
	end
feature -- CRITICAL-LEVEL LOGS
	-- THESE LOGS WILL HAVE THEIR WHOLE MESSAGE IN THE LOG'S COLOR

	log_warn_crit(msg: STRING_8)
		-- LOGS A WARNING TO THE TERMINAL IN CRITICAL MODE; WHOLE MESSAGE WILL CONTAIN COLOR
	do
		if ansi_control.ansi_enabled then
			ansi_control.set_foreground_color_to_yellow
			ansi_control.set_bold
			print ("WARN: ")
			print (msg.out + "%N")
			ansi_control.reset_foreground_color
			ansi_control.reset_attributes -- not sure if its needed but might help ?
		else
			print ("WARN: " + msg.out + "%N")
		end
	end

	log_info_crit(msg: STRING_8)
		-- LOGS USEFUL INFO TO THE TERMINAL IN CRITICAL MODE; WHOLE MESSAGE WILL CONTAIN COLOR
	do
		if ansi_control.ansi_enabled then
			ansi_control.set_foreground_color_to_cyan
			ansi_control.set_bold
			print ("INFO: ")
			print (msg.out + "%N")
			ansi_control.reset_foreground_color
			ansi_control.reset_attributes
		else
			print ("INFO: " + msg.out + "%N")
		end
	end

	log_fail_crit(msg: STRING_8)
		-- LOGS A FAILED OPERATION TO THE TERMINAL IN CRITICAL MODE; WHOLE MESSAGE WILL CONTAIN COLOR
	do
		if ansi_control.ansi_enabled then
			ansi_control.set_foreground_color_to_magenta
			ansi_control.set_bold
			print ("CRITICAL: ")
			print (msg.out + "%N")
			ansi_control.reset_foreground_color
			ansi_control.reset_attributes
		else
			print ("CRITICAL: " + msg.out + "%N")
		end
	end

	log_err_crit(msg: STRING_8)
		-- LOGS A OPERATIONAL ERROR TO THE TERMINAL IN CRITICAL MODE; WHOLE MESSAGE WILL CONTAIN COLOR
	do
		if ansi_control.ansi_enabled then
			ansi_control.set_foreground_color_to_red
			ansi_control.set_bold
			print ("FATAL: ")
			print (msg.out + "%N")
			ansi_control.reset_foreground_color
			ansi_control.reset_attributes
		else
			print ("ERROR: " + msg.out + "%N")
		end
	end

	log_succ_crit(msg: STRING_8)
		-- LOGS A SUCCESSFUL OPERATION TO THE TERMINAL IN CRITICAL MODE; WHOLE MESSAGE WILL CONTAIN COLOR
	do
		if ansi_control.ansi_enabled then
			ansi_control.set_foreground_color_to_green
			ansi_control.set_bold
			print ("SUCCESS: ")
			print (msg.out + "%N")
			ansi_control.reset_foreground_color
			ansi_control.reset_attributes
		else
			print ("SUCCESS: " + msg.out + "%N")
		end
	end
	log_load_crit(msg: STRING_8)
		-- LOGS A LOADING/PROGRESS BAR OPERATION TO THE TERMINAL IN CRITICAL MODE; WHOLE MESSAGE WILL CONTAIN COLOR
	do
		if ansi_control.ansi_enabled then
			ansi_control.set_foreground_color_to_white
			ansi_control.set_background_color_to_green
			ansi_control.set_bold
			print ("LOADING: ")
			print (msg.out + "%N")
			ansi_control.reset_foreground_color
			ansi_control.reset_attributes
		else
			print ("LOADING: " + msg.out + "%N")
		end
	end
feature -- handy dandies
	clr_scr
	do
		if ansi_control.ansi_enabled then
			ansi_control.erase_display
		end
	end

feature -- attribtutes
	ansi_control: CONSOLE_ANSI_CONTROL
		once
			-- ?? only returns a properly set ansi_control because compiler keeps complaining and im too tired
			-- to understand what it wants except from forcing it to return a value that physically cant be not set
			if ansi_control /= void then
				result := ansi_control
			else
				create Result.default_create
			end
		end


invariant
--	ansi_control_init: ansi_control.ansi_enabled /= false	-- old attempt at ansi_control being fucky with compiler

end
