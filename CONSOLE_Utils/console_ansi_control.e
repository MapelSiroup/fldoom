note
	description: "CONSOLE_ANSI_CONTROL Supplies the according character escapes in easy to use functions for coloring and font control within the windows terminal."
	date: "1-21-2024"
	revision: "1.0"
	EIS: "name=ANSI escape codes", "protocol=URI", "src=http://ascii-table.com/ansi-escape-sequences.php"
	EIS: "name=ANSI escape codes (MS)", "protocol=URI", "src=https://docs.microsoft.com/en-us/windows/console/console-virtual-terminal-sequences"

class
	CONSOLE_ANSI_CONTROL

inherit
	ANY
		redefine
			default_create
		end

create
	default_create

feature -- Initialization

	default_create
		local
			i: INTEGER
		do
			io.output.end_of_file.do_nothing
			i := initialize_terminal
			ansi_enabled := (i = 0)
		end

feature -- WINDOWS FIX
	------------------------------------------
	-- PLATFORM `WINDOWS_OS` SPECIFIC
	-- CONSOLE FIX | WINDOWS ONLY | #START
	------------------------------------------	

	------------------------------------
	-- 	THIS SECTION OF CODE IS ONLY TO
	--  MAKE IT SO THAT WINDOWS ALLOWS
	--  ANSI ESCAPE CODES FOR COLOR AND
	--  "RICH" TEXT WITHIN THE DEFAULT
	--  WINDOWS CONSOLE/TERMINAL WINDOW
	------------------------------------

	initialize_terminal: INTEGER
	    external "C inline"
	    alias "[
	    	#ifndef ENABLE_VIRTUAL_TERMINAL_PROCESSING
			#define ENABLE_VIRTUAL_TERMINAL_PROCESSING 0x0004
			#endif
	        #ifdef EIF_WINDOWS
	            {
	                HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
	                if (hOut == INVALID_HANDLE_VALUE) {
	                	return GetLastError();
	                }
	                DWORD dwMode = 0;
	                if (!GetConsoleMode(hOut, &dwMode)) {
	                	return GetLastError();
	                }
	                dwMode |= ENABLE_VIRTUAL_TERMINAL_PROCESSING;
	                if (!SetConsoleMode(hOut, dwMode)) {
        				return GetLastError();
    				}
    				return 0;
	            }
		#else
		      /* Check for other platforms */
		    return 0;
	        #endif
	    ]"
	    end

		------------------------------------------
		-- PLATFORM `WINDOWS_OS` SPECIFIC #END
		------------------------------------------

	ansi_enabled: BOOLEAN

feature -- Control

	set_control (m: READABLE_STRING_8)
		do
			if ansi_enabled then
				io.output.put_string ("%/27/[")
				io.output.put_string (m)
			end
		end

feature -- Control: attributes

	reset_attributes do set_control ("0m") end

	set_bold 			do set_control ("1m") end
	set_underline 		do set_control ("4m") end
	unset_underline		do set_control ("24m") end
	set_blink 			do set_control ("5m") end
	set_reverse_video 	do set_control ("7m") end
	unset_reverse_video do set_control ("27m") end
	set_concealed 		do set_control ("9m") end

feature -- Control: foreground colors		

	reset_foreground_color				do set_control ("39m") end

	set_foreground_color_to_black 		do set_control ("30m") end
	set_foreground_color_to_red 		do set_control ("31m") end
	set_foreground_color_to_green 		do set_control ("32m") end
	set_foreground_color_to_yellow 		do set_control ("33m") end
	set_foreground_color_to_blue 		do set_control ("34m") end
	set_foreground_color_to_magenta 	do set_control ("35m") end
	set_foreground_color_to_cyan 		do set_control ("36m") end
	set_foreground_color_to_white 		do set_control ("37m") end

feature -- Control: background colors		

	reset_background_color				do set_control ("49m") end

	set_background_color_to_black 		do set_control ("40m") end
	set_background_color_to_red 		do set_control ("41m") end
	set_background_color_to_green 		do set_control ("42m") end
	set_background_color_to_yellow 		do set_control ("43m") end
	set_background_color_to_blue 		do set_control ("44m") end
	set_background_color_to_magenta 	do set_control ("45m") end
	set_background_color_to_cyan 		do set_control ("46m") end
	set_background_color_to_white 		do set_control ("47m") end

feature -- Control

	move_cursor_to_column (a_column: INTEGER)
		require
			valid_column: a_column >= 0 and a_column <= 32_767
		do
			set_control (a_column.out + "G")
		end

	move_cursor_to_line (a_line: INTEGER)
		require
			valid_line: a_line >= 0 and a_line <= 32_767
		do
			set_control (a_line.out + "d")
		end

	move_cursor_to_position (a_line, a_column: INTEGER) do set_control (a_line.out + ";" + a_column.out + "H") end
	move_cursor_up (nb: INTEGER) do set_control (nb.out + "A") end
	move_cursor_down (nb: INTEGER) do set_control (nb.out + "B") end
	move_cursor_forward (nb: INTEGER) do set_control (nb.out + "C") end
	move_cursor_backward (nb: INTEGER) do set_control (nb.out + "D") end
	save_cursor do set_control ("s") end
	restore_cursor do set_control ("u") end

	show_cursor do set_control ("?25h") end
	hide_cursor do set_control ("?25l") end

	erase_display_from_cursor do set_control ("0J") end -- from the cursor position (inclusive) up to the end of the display
	erase_display_until_cursor do set_control ("1J") end -- from the beginning of the display up to the cursor (including the cursor position)
	erase_display do set_control ("2J") end

	erase_line_from_cursor do set_control ("0K") end -- from the cursor position (inclusive) up to the end of the line
	erase_line_until_cursor do set_control ("1K") end -- from the beginning of the line up to the cursor (including the cursor position)	
	erase_line do set_control ("2K") end

feature -- Modes

	enable_line_wrapping_mode do set_control ("=7h") end
	reset_line_wrapping_mode do set_control ("=l") end

end
