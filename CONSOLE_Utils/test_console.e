note
    description : "TEST CONSOLE PROTOTYPE FOR ANSI COLORING DIRECTLY INTO TERMINAL"
    author      : "Florent Perreault, forked from @phgachoud Gachoud Philippe's code snippet"
	see_also	: "https://gist.github.com/phgachoud/c8b6a07138b6961e89f86797dde8340e"
    date        : "2024-01-19"
    revision    : "1.0"

class
	TEST_CONSOLE

inherit
	SHARED_EXECUTION_ENVIRONMENT

create
	make

feature {NONE} -- Initialization

	make
			-- Instantiate Current object.
		local
			ansi_control: CONSOLE_ANSI_CONTROL
			n: INTEGER
		do
			create ansi_control
			ansi_control.erase_display

			print ("This demonstrates the use of ANSI control on terminal%N%N")

			print ("Hello")
			ansi_control.save_cursor
			ansi_control.set_foreground_color_to_red
			print (" Eiffel!")
			print ("%N")

			n := 10

			ansi_control.hide_cursor
			ansi_control.move_cursor_to_column (n + 1)
			ansi_control.set_foreground_color_to_cyan
			print (create {STRING}.make_filled ('_', n))

			ansi_control.move_cursor_to_column (1)
			print ("...")

			across
				1 |..| n as ic
			loop
				ansi_control.erase_line_until_cursor
				ansi_control.set_foreground_color_to_yellow
				ansi_control.move_cursor_to_column (n + ic.item)
				print ("#")

				ansi_control.move_cursor_to_column (1)
				ansi_control.set_foreground_color_to_green
				print (ic.item)
				execution_environment.sleep (1_000_000_000 // n)
			end
			ansi_control.erase_line_until_cursor
			ansi_control.move_cursor_to_column (1)
			print ("Done%N")
			ansi_control.erase_line
			ansi_control.set_background_color_to_white
			ansi_control.set_foreground_color_to_blue
			ansi_control.show_cursor
			ansi_control.restore_cursor
			ansi_control.move_cursor_backward (1)
			ansi_control.erase_line_until_cursor
			ansi_control.move_cursor_to_column (1)
			print ("Bye")
			ansi_control.reset_background_color
			ansi_control.reset_foreground_color

			ansi_control.move_cursor_to_position (6, 1)
			print ("See you ")
			from
				n := 40
			until
				n = 1
			loop
				ansi_control.move_cursor_to_column (8)
				ansi_control.erase_display_from_cursor
				ansi_control.move_cursor_to_column (7 + n)
				print ("soon!")
				execution_environment.sleep (10_000_000)
				n := n - 1
			end
			print ("%N")
		end

end
