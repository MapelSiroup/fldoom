note
	description: "Summary description for {ENGINE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ENGINE
inherit
	GAME_LIBRARY_SHARED
--	GAMEMODE_SHARED	-- not yet implemented
	AUDIO_LIBRARY_SHARED

create
	make,
	make_with_maps

feature {NONE} -- Initialization
	old_timestamp:NATURAL_32
			-- last `update` iteration
	make
			-- Initialization of `Current'
		local
			l_window_builder:GAME_WINDOW_RENDERED_BUILDER
		do
			create m_player.make(1)
			create map_list.make(0)
			old_timestamp := 0
			create l_window_builder
			l_window_builder.set_dimension (1024, 512)	-- Width, Height
			l_window_builder.set_title ("FLDoom")
			l_window_builder.enable_must_renderer_synchronize_update	-- Ask to the video card to manage the frame synchronisation (FPS)
			window := l_window_builder.generate_window
		end

	make_with_maps(a_maps: ARRAYED_LIST[MAP])
			-- Initialization of `Current'
		local
			l_window_builder:GAME_WINDOW_RENDERED_BUILDER
		do

			map_list := a_maps
			m_player := map_list[1].m_player
			old_timestamp := 0
			create l_window_builder
			l_window_builder.set_dimension (1024, 512)	-- Width, Height
			l_window_builder.set_title ("FLDoom")
			l_window_builder.enable_resizable	-- added 3/31/2024 TODO: check if it breaks stuff
			l_window_builder.enable_must_renderer_synchronize_update	-- Ask to the video card to manage the frame synchronisation (FPS)
			window := l_window_builder.generate_window
		end

feature -- Access

	run
			-- Launch the game once we have loaded necessary files.
		require
			No_Error: not has_error
		do

			game_library.quit_signal_actions.extend (agent on_quit)
			window.key_pressed_actions.extend (agent on_key_pressed)
			window.key_released_actions.extend (agent on_key_released)
			game_library.iteration_actions.extend (agent on_iteration)
			if window.renderer.driver.is_present_synchronized_supported then	-- If the Video card accepted the frame synchronisation (FPS)
				game_library.launch_no_delay									-- Don't let the library manage the frame synchronisation
			else
				game_library.launch
			end


		end


	has_error:BOOLEAN
			-- `True' if an error occured during the creation of `Current'

	window:GAME_WINDOW_RENDERED
			-- The window to draw the scene

	map_list: ARRAYED_LIST[MAP]
			-- the iwad loader
	m_player: PLAYER

feature {NONE} -- Implementation

	on_iteration(a_timestamp:NATURAL_32)
			-- Event that is launched at each `update` iteration, using the next iteration step number `a_timestamp`.
		do
			m_player.mouvement_update(a_timestamp, old_timestamp)
--			across 1 |..| 474 as linedefs loop
--				window.renderer.draw_line (map_list[1].m_vertexes[map_list[1].m_linedefs[linedefs.item].start_vertex].x,
--										   map_list[1].m_vertexes[map_list[1].m_linedefs[linedefs.item].start_vertex].y,
--										   map_list[1].m_vertexes[map_list[1].m_linedefs[linedefs.item].end_vertex].x,
--										   map_list[1].m_vertexes[map_list[1].m_linedefs[linedefs.item].end_vertex].y)
--			end
			---------------------draw_map------------------------
			if map_list.first /= Void then
				window.renderer.set_drawing_color (create {GAME_COLOR}.make_rgb (0, 0, 0))
				window.renderer.clear	-- redraw so that we dont have hall of mirrors
				map_list.first.render_automap (window.renderer)
			end
			-----------------------------------------------------
			-- Update titlebar
			window.set_title ("FLDoom " + "FPS: " + (1/((a_timestamp - old_timestamp)/1000)).rounded.out + 
							  " POS[" + m_player.m_position_x.out + "," + m_player.m_position_y.out + "]" +
							  " PLAYER DIR: " + m_player.m_angle.out)
			--Update window renderer
			window.renderer.present		-- Update modification in the screen
			old_timestamp := a_timestamp	--Set old_timestamp to current timestamp
			audio_library.update
		end

	on_key_pressed(a_timestamp: NATURAL_32; a_key_event: GAME_KEY_EVENT)
			-- Action when a keyboard key has been pushed
			-- @params a_timestamp : le timestamp courrant
			-- @params a_key_event : SDL2 key event (key code)
		do
			if not a_key_event.is_repeat then		-- Be sure that the event is not only an automatic repetition of the key
				if a_key_event.is_w then
					-- move forward
					m_player.set_moveforward(true)
				end
				if a_key_event.is_a then
					-- strafe left
					m_player.set_strafeleft(true)
				end
				if a_key_event.is_s then
					-- move back
					m_player.set_movebackward(true)
				end
				if a_key_event.is_d then
					-- strafe right
					m_player.set_straferight(true)
				end
				if a_key_event.is_right then
					-- look right
					m_player.set_turnright(true)
				end
				if a_key_event.is_left then
					-- look left
					m_player.set_turnleft(true)
				end
				if a_key_event.is_up then
					-- look up
				end
				if a_key_event.is_down then
					-- look down
				end
				if a_key_event.is_space then
					-- jump
				end
				if a_key_event.is_left_ctrl then
					-- crouch
				end
			end
		end

	on_key_released(a_timestamp: NATURAL_32; a_key_event: GAME_KEY_EVENT)
			-- Action when a keyboard key has been released
			-- @params a_timestamp : le timestamp courrant
			-- @params a_key_event : SDL2 key event (key code)
		do
			if not a_key_event.is_repeat then		-- Be sure that the event is not only an automatic repetition of the key
				if a_key_event.is_w then
					-- move forward
					m_player.set_moveforward(false)
				end
				if a_key_event.is_a then
					-- strafe left
					m_player.set_strafeleft(false)
				end
				if a_key_event.is_s then
					-- move back
					m_player.set_movebackward(false)
				end
				if a_key_event.is_d then
					-- strafe right
					m_player.set_straferight(false)
				end
				if a_key_event.is_right then
					-- stop looking right
					m_player.set_turnright(false)
				end
				if a_key_event.is_left then
					-- stop looking left
					m_player.set_turnleft (false)
				end
				if a_key_event.is_up then

				end
				if a_key_event.is_down then

				end
				if a_key_event.is_space then
					--jump stop?
				end
				if a_key_event.is_left_ctrl then
					-- crouch
				end
			end
		end

	on_quit(a_timestamp: NATURAL_32)
			-- This method is called when the quit signal is sent to the application (ex: window X button pressed).
		do
			game_library.stop  -- Stop the controller loop (allow game_library.launch to return)
		end
end
