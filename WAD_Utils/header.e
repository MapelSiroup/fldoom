note
	description: "The header class contains valuable WAD information such as directory size, lumps count and WAD Type."
	author: "Florent Perreault"
	date: "3/31/2024"
	revision: "1.0.1"

class
	HEADER

create
    make

feature -- Constants

    BYTE_SIZE: INTEGER = 1
    INTEGER_SIZE: INTEGER = 4

feature -- Attributes

    wad_type: STRING ASSIGN set_wad_type
    directory_count: NATURAL_32 ASSIGN set_directory_count
    directory_offset: NATURAL_32 ASSIGN set_directory_offset

feature -- Initialization

    make
        -- Initialization if needed
    do
        -- Initialization code if needed
		wad_type := "NULL"

    end

feature -- Setters

    set_wad_type (a_wad_type: STRING)
        -- Assign `wad_type' with `a_wad_type'
        do
            wad_type := a_wad_type
        ensure
            is_assign: wad_type = a_wad_type
        end

    set_directory_count (a_directory_count: NATURAL_32)
        -- Assign `directory_count' with `a_directory_count'
        do
            directory_count := a_directory_count
        ensure
            is_assign: directory_count = a_directory_count
        end

    set_directory_offset (a_directory_offset: NATURAL_32)
        -- Assign `directory_offset' with `a_directory_offset'
        do
            directory_offset := a_directory_offset
        ensure
            is_assign: directory_offset = a_directory_offset
        end

end -- class HEADER
