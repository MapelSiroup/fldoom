note
	description: "Summary description for {WAD_LOADER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	WAD_LOADER

inherit
	CONSOLE_LOGGER

create
	make

feature {NONE} -- Initialization

	 make (a_wad_file_path: STRING)
			-- Initialization for `Current'.
        do
            m_s_wad_file_path := a_wad_file_path
            create {ARRAYED_LIST [NATURAL_8]} m_wad_data.make(0)
            create {ARRAYED_LIST [WAD_DIRECTORY]} m_wad_directories.make(0)
--			m_wad_file.make_with_path (m_s_wad_file_path)
			create reader
			create header.make
			create map_list.make (0)
        end


feature -- Access

    wad_file_path: STRING
        do
            Result := m_s_wad_file_path
        end

--    wad_file: FILE
--        do
--            Result := m_wad_file
--        end

--    wad_data: ARRAYED_LIST [NATURAL_8]

    wad_directories: LIST [WAD_DIRECTORY]
        do
            Result := m_wad_directories
        end

feature -- Operations

load_wad: BOOLEAN
        local
            success: BOOLEAN
            l_e1m1: MAP
            l_player: PLAYER
        do
        	success := false
            if open_and_load then
            	log_info ("Opening of the WAD is successful, Beginning reading directories...")
                success := read_directories
                if success then
                	create l_player.make(1)
                	create l_e1m1.make("E1M1",l_player)
                	log_info ("Processing MAP Vertex...")
                	l_e1m1 := read_map_vertexes (l_e1m1)
                	log_info ("Processing MAP Linedef...")
                	l_e1m1 := read_map_linedefs (l_e1m1)
                	log_info ("Processing MAP Things...")
                	l_e1m1 := read_map_things   (l_e1m1)
                	log_info ("Processing MAP Nodes...")
                	l_e1m1 := read_map_nodes    (l_e1m1)
                	log_info ("Processing MAP Subsectors...")
                	l_e1m1 := read_map_subsectors (l_e1m1)
                	log_info ("Processing MAP Segments...")
                	l_e1m1 := read_map_segs (l_e1m1)
					map_list.extend (l_e1m1)
                	log_info("Reading of the MAP lumps was successful.")
                else
                	log_fail("Reading of the WAD lumps was unsuccessful. Is your WAD file following DOOM 1 format?")
                end
            else
            	log_err("Reading and opening of the WAD was unsuccessful. Does your WAD file has READ/WRITE permissions?")
            end

            Result := success
        end

feature -- Internal operations

    open_and_load: BOOLEAN
        local
            length: INTEGER
        do
            log_info("Attempting the Opening and Loading of WAD file: " + m_s_wad_file_path)
			m_wad_data := reader.read_wad (m_s_wad_file_path)
			length := m_wad_data.count
			if length > 0 then
				log_succ("Read successfully "+(length-1).out+" BYTES FROM "+m_s_wad_file_path)
				result := true
			else
				log_fail_crit("Failed to open WAD file @"+ m_s_wad_file_path+" of length= "+m_wad_data.count.out)
                Result := False
			end
        end

    read_directories: BOOLEAN
        local
            i: INTEGER
        do
			-- i dont know if i should be allowed to not check if the header is valid but im not sure if i could make a bool operation out of it soooo ill just go without protection for now and read into memory cuz why not.
            reader.read_header_data (m_wad_data, 1, header)
            log_info("Parsing HEADER data...")
            log_info("Fetched all the WAD's header data, fetching and exploring directories...")
			---------------------------
			--	DIRECTORIES
			---------------------------
        	if attached header and then attached reader then
--        		if (header.directory_count < m_wad_data.count.to_natural_32 or header.directory_offset <= m_wad_data.count.to_natural_32) then
        	        from
                        i := 0
                    until
                        i > (header.directory_count.to_integer_32-1)	--- fix this it could be an error that we are reading it wrong because of the bit smashing done in read_4_bytes we should check this value carefully
                    loop
                    	-- wtf is this i dont remember this check and too sleepy to figure why it would be needed
--                    	if not (header.directory_offset < m_wad_data.count.to_natural_32) then
--                    		result := false
--                    	end

--                    	log_info("directory offset: "+header.directory_offset.out+", "+(header.directory_offset.to_integer_32+i).to_natural_32.to_hex_string+", "+(header.directory_offset.to_integer_32+(i*16)).out)
						m_wad_directories.extend (reader.read_directory_data (m_wad_data, (header.directory_offset.to_integer_32 + (i * 16))))
--						log_info("Directory at offset= 0x"+directory.lump_offset.to_hex_string+", size="+directory.lump_size.to_hex_string)
                        i := i + 1
                    end
                    log_info("All Lumps fetched! Loading collected data ...")
                	Result := True
--                else
--                	log_err_crit ("TOO MANY DIRECTORIES, FATAL: out of indices for array length: "+m_wad_data.count.out+" index: "+header.directory_offset.out)
--        	    	Result := False
--        	    end
        	else
        		log_warn_crit ("HEADER is detached, make sure the HEADER class is invariant and always attached before passing information to the READER class.")
        		Result := False
        	end
        end

		read_map_vertexes(a_map:MAP):MAP
		local
			 l_map_index: INTEGER
			l_vertices_count, i: NATURAL_32
		do
			l_map_index := find_lump_index_from_name(a_map.m_name)

			if l_map_index = 0 then
				log_err("map index is 0??")
				create result.make("",create {PLAYER}.make (0))
			end

			l_map_index := (l_map_index + {E_MAP_LUMPS_INDEX}.vertexes)
			log_info("lump_size in bytes: "+ m_wad_directories[l_map_index].lump_size.out)
			l_vertices_count := (m_wad_directories[l_map_index].lump_size//4) -- reason for magic number: BYTE_SIZE * (2^2) because we will read 2 bytes of BYTE_SIZE*2 each. this way we dont need unnecessary multiplication for a simple number like 4 in this case
			from
				i := 0
			until
				i > (l_vertices_count - 1)
			loop
				a_map.add_vertex (reader.read_vertex_data(m_wad_data, (m_wad_directories[l_map_index].lump_offset + (i*4)).to_integer_32 )) -- line is working now, compiler had trouble with some types
				i := i + 1
			end
			log_info("vert count: "+ l_vertices_count.out)
--			print(m_wad_directories[l_map_index].lump_offset.out+"%N")
--			across a_map.m_vertexes as vertices loop print(vertices.item.to_string+"%N") end
			result := a_map
		end

		read_map_linedefs(a_map:MAP):MAP
		local
			l_map_index: INTEGER
			l_linedefs_count, i: NATURAL_32
		do
			l_map_index := find_lump_index_from_name(a_map.m_name)

			if l_map_index = 0 then
				log_err("map index is 0??")
				create result.make("",create {PLAYER}.make(0))
			end

			l_map_index := (l_map_index + {E_MAP_LUMPS_INDEX}.linedefs)
			log_info("lump_size in bytes: "+ m_wad_directories[l_map_index].lump_size.out)
			l_linedefs_count := (m_wad_directories[l_map_index].lump_size//14) -- reason for magic number: BYTE_SIZE * (7*2) because we read 14 bytes of BYTE_SIZE each. this way we dont need unnecessary multiplication for a simple number like 14 in this case
			from
				i := 0
			until
				i > (l_linedefs_count - 1)
			loop
				a_map.add_linedef(reader.read_linedef_data(m_wad_data, (m_wad_directories[l_map_index].lump_offset + (i*14)).to_integer_32 )) -- line is working now, compiler had trouble with some types
				i := i + 1
			end
			log_info("linedefs count: "+ (l_linedefs_count - 1).out)
--			print(m_wad_directories[l_map_index].lump_offset.out+"%N")
			result := a_map
		end

		read_map_things(a_map:MAP):MAP
		local
			l_map_index: INTEGER
			l_things_count, i: NATURAL_32
		do
			l_map_index := find_lump_index_from_name(a_map.m_name)

			if l_map_index = 0 then
				log_err("map index is 0??")
				create result.make("",create {PLAYER}.make (0))
			end

			l_map_index := (l_map_index + {E_MAP_LUMPS_INDEX}.things)
			log_info("lump_size in bytes: "+ m_wad_directories[l_map_index].lump_size.out)
			l_things_count := (m_wad_directories[l_map_index].lump_size//10) -- reason for magic number: BYTE_SIZE * (5*2) because we read 10 bytes of BYTE_SIZE each. this way we dont need unnecessary multiplication for a simple number like 10 in this case
			from
				i := 0
			until
				i > (l_things_count - 1)
			loop
				a_map.add_thing(reader.read_thing_data(m_wad_data, (m_wad_directories[l_map_index].lump_offset + (i*10)).to_integer_32 ))
				i := i + 1
			end
			log_info("things count: "+ (l_things_count - 1).out)
--			print(m_wad_directories[l_map_index].lump_offset.out+"%N")
			result := a_map
		end

		read_map_nodes(a_map:MAP):MAP
		local
			l_map_index: INTEGER
			l_nodes_count, i: NATURAL_32
		do
			l_map_index := find_lump_index_from_name(a_map.m_name)

			if l_map_index = 0 then
				log_err("map index is 0??")
				create result.make("",create {PLAYER}.make (0))
			end

			l_map_index := (l_map_index + {E_MAP_LUMPS_INDEX}.nodes)
			log_info("lump_size in bytes: "+ m_wad_directories[l_map_index].lump_size.out)
			l_nodes_count := (m_wad_directories[l_map_index].lump_size//28)
			from
				i := 0
			until
				i > (l_nodes_count - 1)
			loop
				a_map.add_node(reader.read_node_data(m_wad_data, (m_wad_directories[l_map_index].lump_offset + (i*28)).to_integer_32 ))
				i := i + 1
			end
			log_info("nodes count: "+ (l_nodes_count - 1).out)
--			print(m_wad_directories[l_map_index].lump_offset.out+"%N")
			result := a_map
		end

		read_map_subsectors(a_map:MAP):MAP
		local
			l_map_index: INTEGER
			l_subsectors_count, i: NATURAL_32
		do
			l_map_index := find_lump_index_from_name(a_map.m_name)
			if l_map_index = 0 then
				log_err("map index is 0??")
				create result.make("",create {PLAYER}.make (0))
			end
			l_map_index := (l_map_index + {E_MAP_LUMPS_INDEX}.ssectors)
			log_info("lump_size in bytes: "+ m_wad_directories[l_map_index].lump_size.out)
			l_subsectors_count := (m_wad_directories[l_map_index].lump_size//4)
			from
				i := 0
			until
				i > (l_subsectors_count - 1)
			loop
				a_map.add_ssector(reader.read_subsector_data(m_wad_data, (m_wad_directories[l_map_index].lump_offset + (i*4)).to_integer_32 ))
				i := i + 1
			end
			log_info("subsectors count: "+ (l_subsectors_count - 1).out)
--			print(m_wad_directories[l_map_index].lump_offset.out+"%N")
			result := a_map
		end

		read_map_segs(a_map:MAP):MAP
		local
			l_map_index: INTEGER
			l_segs_count, i: NATURAL_32
		do
			l_map_index := find_lump_index_from_name(a_map.m_name)
			if l_map_index = 0 then
				log_err("map index is 0??")
				create result.make("",create {PLAYER}.make (0))
			end
			l_map_index := (l_map_index + {E_MAP_LUMPS_INDEX}.segs)
			log_info("lump_size in bytes: "+ m_wad_directories[l_map_index].lump_size.out)
			l_segs_count := (m_wad_directories[l_map_index].lump_size//12)
			from
				i := 0
			until
				i > (l_segs_count - 1)
			loop
				a_map.add_segment(reader.read_seg_data(m_wad_data, (m_wad_directories[l_map_index].lump_offset + (i*12)).to_integer_32 ))
				i := i + 1
			end
			log_info("segments count: "+ (l_segs_count - 1).out)
--			print(m_wad_directories[l_map_index].lump_offset.out+"%N")
			result := a_map
		end

feature -- handy dandies

        find_lump_from_map(a_map:MAP): WAD_DIRECTORY
        do
        	across m_wad_directories as lumps loop
        		--print(lumps.item.lump_name.out)
        		if lumps.item.lump_name.out.substring_index (a_map.m_name, 1) /= 0 then
        			result := lumps.item
        			log_info("Found lump compatible with map name "+ lumps.item.lump_name +", from name "+ a_map.m_name)
        		end
--        		m_wad_directories.index_of(a_map.m_name, lumps)
        	end
        	if result = void then
        		log_fail("Couldn't find lump of name: "+a_map.m_name+", returned empty lump.")
        		create result.make_empty
        	end
        end

		find_lump_index_from_name(a_lumpname:STRING_8): INTEGER
        local
			i: INTEGER
			break: BOOLEAN
        do
        	from
        		i := 1; break := false
        	until
        	 	(i > m_wad_directories.count) or break
        	loop
        		--print(m_wad_directories[i].lump_name)
        		if m_wad_directories[i].lump_name.substring_index (a_lumpname, 1) /= 0 then
        			log_info("Found lump compatible with map name "+ m_wad_directories[i].lump_name +", from name "+ a_lumpname+" lump index: "+ (i-1).out)
        			result := i-1
        			break := true
        		end
        		i := i + 1
        	end
        	if result < 0 or not break then
        		log_fail("Couldn't find lump of name: "+a_lumpname+", returned 0'd index.")
        		result := -1
        	end
--        	across m_wad_directories as lumps loop
--        		if lumps.item.name = a_map.m_name then
--        			result := lumps.item
--        			
--        		end
--        		m_wad_directories.index_of (like m_wad_directories.item.lump_name = a_lumpname, 1)
--        	end

        end

feature -- Finalization

    dispose
        do
            -- Clean up resources (if any)
        end

feature -- Attributes

    m_s_wad_file_path: STRING
--    m_wad_file: RAW_FILE
    m_wad_data: ARRAYED_LIST [NATURAL_8]
    m_wad_directories: LIST [WAD_DIRECTORY]
    reader: WAD_READER
    header: HEADER
    map_list: ARRAYED_LIST[MAP]

invariant
	valid_reader: reader /= Void
	valid_header: header /= void
--    valid_wad_file_path: not m_s_wad_file_path.is_empty
--    valid_wad_file: not m_wad_file.is_closed
--    valid_wad_data: m_wad_data.count > 0
--    valid_wad_directories: m_wad_directories.lower >= 1

end	-- class WAD_LOADER
