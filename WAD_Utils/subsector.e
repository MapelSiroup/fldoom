note
	description: "Class defining the Subsectors within a level/map"
	author: "Florent Perreault"
	date: "2/29/2024"
	revision: "1.0.0"

class
	SUBSECTOR

create
    make

feature -- Attributes
    seg_count: NATURAL_16
    first_seg_id: NATURAL_16

feature -- Initialization

    make (a_seg_count, a_first_seg_id: NATURAL_16)
        do
            seg_count := a_seg_count
            first_seg_id := a_first_seg_id
        end

end
