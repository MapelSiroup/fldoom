note
	description: "Summary description for {SIDEDEF}."
	author: "Florent Perreault"
	date: "$date$"
	revision: "$Revision$"

class
	SIDEDEF

create
    make

feature -- Attributes

    offset_x: INTEGER
    offset_y: INTEGER
    upper_texture_name: STRING
    lower_texture_name: STRING
    middle_texture_name: STRING
  	sector: SECTOR
--  upper_texture: detachable TEXTURE
--  lower_texture: detachable TEXTURE
--  middle_texture: detachable TEXTURE
    use_sky_hack: BOOLEAN

feature -- Initialization

    make (a_offset_x, a_offset_y: INTEGER; an_upper_texture_name, a_lower_texture_name, a_middle_texture_name: STRING)--; a_sector: SECTOR
       -- ; an_upper_texture, a_lower_texture, a_middle_texture: detachable TEXTURE) is
        do
            offset_x := a_offset_x
            offset_y := a_offset_y
            upper_texture_name := an_upper_texture_name
            lower_texture_name := a_lower_texture_name
            middle_texture_name := a_middle_texture_name
--            sector := a_sector
--            upper_texture := an_upper_texture
--            lower_texture := a_lower_texture
--            middle_texture := a_middle_texture
        end

feature -- Accessors

    is_use_sky_hack: BOOLEAN
        do
            Result := use_sky_hack
        end

    set_use_sky_hack (a_use_sky_hack: BOOLEAN)
        do
            use_sky_hack := a_use_sky_hack
        end

end -- class SIDEDEF
