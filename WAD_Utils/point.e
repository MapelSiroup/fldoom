note
	description: "Summary description for {POINT}."
	author: "FLORENT PERREAULT"
	date: "1/19/2023"
	revision: "$Revision$"

class
	POINT
create
    make,
    make_point,
    make_from_point

feature -- Attributes

    x: INTEGER_16 assign set_x
    y: INTEGER_16 assign set_y

feature -- Initialization

    make
        do
            x := 0
            y := 0
        end

    make_point (a_x, a_y: INTEGER_16)
        do
            x := a_x
            y := a_y
        end

    make_from_point (p: POINT)
        do
            x := p.x
            y := p.y
        end

feature -- Operations

    distance (p: POINT): DOUBLE
        do
            Result := {DOUBLE_MATH}.sqrt(((p.x - x) * (p.x - x)) + ((p.y - y) * (p.y - y)))
            -- eiffel doesnt understand the double asterix notation for exponentiation
            --Result := sqrt((p.x - x) ** 2 + (p.y - y) ** 2)
        end

    translate (dx, dy: INTEGER_16)
        do
            x := x + dx
            y := y + dy
        end

    translate_by_vector (v: VEC2)
        do
            x := (x + v.x.rounded).to_integer_16
            y := (y + v.y.rounded).to_integer_16
        end

    to_vec2: VEC2
        do
            create Result.make_vec2 (x, y)
        end

    get_location: POINT
        do
            create Result.make_from_point (Current)
        end

    get_x: INTEGER_16
        do
            Result := x
        end

    get_y: INTEGER_16
        do
            Result := y
        end

    move (a_x, a_y: INTEGER_16)
        do
            x := a_x
            y := a_y
        end

    set_location (a_x, a_y: INTEGER_16)
        do
            x := a_x
            y := a_y
        end

    set_location_double (a_x, a_y: DOUBLE)
        do
            x := a_x.rounded.to_integer_16
            y := a_y.rounded.to_integer_16
        end

    set_location_point (p: POINT)
        do
            x := p.x
            y := p.y
        end

feature -- Setters
	set_x(a_x: INTEGER_16)
	do
		x := a_x
	ensure
		is_assign: x = a_x
	end

	set_y(a_y: INTEGER_16)
	do
		y := a_y
	ensure
		is_assign: y = a_y
	end

feature -- Output

    to_string: STRING
        do
            create Result.make_from_string ("POINT{x=" + x.out + ", y=" + y.out + "}")
        end

end -- class POINT
