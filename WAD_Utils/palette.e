note
	description: "Summary description for {PALETTE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PALETTE

create
    make

feature -- Class Variables

    colors: ARRAY [INTEGER]

feature -- Initialization

    make
        do
            create colors.make_empty
        end

feature -- Extract Palette

    extract_palette (palette_lump: WAD_DIRECTORY)
        local
--            i, r, g, b, a: INTEGER
        do
            --palette_lump.set_position(0)

--            from
--                i := 1
--            until
--                i > 256
--            loop
--                r := palette_lump.read_byte
--                g := palette_lump.read_byte
--                b := palette_lump.read_byte
--                a := 255
--                colors[i] := b + (g \\ 8) + (r \\ 16) + (a \\ 24)
--                i := i + 1
--            end
        end

end -- class PALETTE
