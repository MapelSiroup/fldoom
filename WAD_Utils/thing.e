note
	description: "Summary description for {THING}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
    THING

create
    make

feature -- Attributes

    position_x: INTEGER_16
    position_y: INTEGER_16
    angle: NATURAL_16
    type: NATURAL_16
    flags: NATURAL_16

feature -- Initialization

    make (a_position_x, a_position_y:INTEGER_16; an_angle, a_type, a_flags: NATURAL_16)
        do
            position_x := a_position_x
            position_y := a_position_y
            angle := an_angle
            type := a_type
            flags := a_flags
        end

end -- class THING

