note
	description: "Summary description for {NODE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	NODE
create
    make

feature -- Initialization

    make (a_x_partition, a_y_partition, a_change_x_partition, a_change_y_partition,
          a_right_box_top, a_right_box_bottom, a_right_box_left, a_right_box_right,
          a_left_box_top, a_left_box_bottom, a_left_box_left, a_left_box_right: INTEGER_16;
          a_right_child_id, a_left_child_id:NATURAL_16)
        do
            x_partition := a_x_partition
            y_partition := a_y_partition
            change_x_partition := a_change_x_partition
            change_y_partition := a_change_y_partition

            right_box_top := a_right_box_top
            right_box_bottom := a_right_box_bottom
            right_box_left := a_right_box_left
            right_box_right := a_right_box_right

            left_box_top := a_left_box_top
            left_box_bottom := a_left_box_bottom
            left_box_left := a_left_box_left
            left_box_right := a_left_box_right

            right_child_id := a_right_child_id
            left_child_id := a_left_child_id
        end

feature -- Access

    x_partition: INTEGER_16
    y_partition: INTEGER_16
    change_x_partition: INTEGER_16
    change_y_partition: INTEGER_16

    right_box_top: INTEGER_16
    right_box_bottom: INTEGER_16
    right_box_left: INTEGER_16
    right_box_right: INTEGER_16

    left_box_top: INTEGER_16
    left_box_bottom: INTEGER_16
    left_box_left: INTEGER_16
    left_box_right: INTEGER_16

    right_child_id: NATURAL_16
    left_child_id: NATURAL_16

feature -- Setters

    set_x_partition (a_x: INTEGER_16)
        do
            x_partition := a_x
        ensure
            is_assign: x_partition = a_x
        end

    set_y_partition (a_y: INTEGER_16)
        do
            y_partition := a_y
        ensure
            is_assign: y_partition = a_y
        end

    set_change_x_partition (a_change_x: INTEGER_16)
        do
            change_x_partition := a_change_x
        ensure
            is_assign: change_x_partition = a_change_x
        end

    set_change_y_partition (a_change_y: INTEGER_16)
        do
            change_y_partition := a_change_y
        ensure
            is_assign: change_y_partition = a_change_y
        end

    set_right_box_top (a_top: INTEGER_16)
        do
            right_box_top := a_top
        ensure
            is_assign: right_box_top = a_top
        end

    set_right_box_bottom (a_bottom: INTEGER_16)
        do
            right_box_bottom := a_bottom
        ensure
            is_assign: right_box_bottom = a_bottom
        end

    set_right_box_left (a_left: INTEGER_16)
        do
            right_box_left := a_left
        ensure
            is_assign: right_box_left = a_left
        end

    set_right_box_right (a_right: INTEGER_16)
        do
            right_box_right := a_right
        ensure
            is_assign: right_box_right = a_right
        end

    set_left_box_top (a_top: INTEGER_16)
        do
            left_box_top := a_top
        ensure
            is_assign: left_box_top = a_top
        end

    set_left_box_bottom (a_bottom: INTEGER_16)
        do
            left_box_bottom := a_bottom
        ensure
            is_assign: left_box_bottom = a_bottom
        end

    set_left_box_left (a_left: INTEGER_16)
        do
            left_box_left := a_left
        ensure
            is_assign: left_box_left = a_left
        end

    set_left_box_right (a_right: INTEGER_16)
        do
            left_box_right := a_right
        ensure
            is_assign: left_box_right = a_right
        end

    set_right_child_id (a_id: NATURAL_16)
        do
            right_child_id := a_id
        ensure
            is_assign: right_child_id = a_id
        end

    set_left_child_id (a_id: NATURAL_16)
        do
            left_child_id := a_id
        ensure
            is_assign: left_child_id = a_id
        end
end
