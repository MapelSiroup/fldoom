note
	description: "Summary description for {WAD_DIRECTORY}."
	author: "FLORENT PERREAULT"
	date: "1/18/2023"
	revision: "$Revision$"

class
	WAD_DIRECTORY

create
    make,
    make_empty

feature -- Initialization

    make (a_offset, a_length: NATURAL_32; a_name: STRING_8; a_data: ARRAY [NATURAL_8])
			-- Initialization for `Current'.
        do
            lump_offset := a_offset
            lump_size := a_length
          	data := a_data
          	lump_name := "N/A"
        end

    make_empty
    	do
    		lump_offset := 0
    		lump_size := 0
    		create data.make_empty
    		lump_name := "N/A"
    	end

feature -- Access



feature -- Constants

	BYTE_SIZE: INTEGER = 1
    INTEGER_SIZE: INTEGER = 4

feature -- Attributes

    lump_offset: NATURAL_32 ASSIGN set_lump_offset
    lump_size: NATURAL_32 ASSIGN set_lump_size
    lump_name: STRING ASSIGN set_lump_name
	data: ARRAY [NATURAL_8]	-- not sure if i wanna keep that tbh
feature -- operations

	is_empty: BOOLEAN
	do
		result := (lump_offset = 0 and lump_size = 0) -- returns the boolean operation
	end

feature -- Setters

    set_lump_offset (a_lump_offset: NATURAL_32)
        -- Assign `lump_offset' with `a_lump_offset'
        do
            lump_offset := a_lump_offset
        ensure
            is_assign: lump_offset = a_lump_offset
        end

    set_lump_size (a_lump_size: NATURAL_32)
        -- Assign `lump_size' with `a_lump_size'
        do
            lump_size := a_lump_size
        ensure
            is_assign: lump_size = a_lump_size
        end

    set_lump_name (a_lump_name: STRING)
        -- Assign `lump_name' with `a_lump_name'
        do
            lump_name := a_lump_name
        ensure
            is_assign: lump_name = a_lump_name
        end

feature -- String representation

    to_string: STRING
        do
            create Result.make_empty
            Result.append ("Directory{offset=" + lump_offset.out + ", length=" + lump_size.out + ", name=" + lump_name + "}") -- + ", data=" + data
        end

	to_string_dataless: STRING
		do
			create Result.make_empty
			Result.append ("Directory(offset="+ lump_offset.out + ", length=" + lump_size.out + ", name=" + lump_name + "}")
		end

invariant
    valid_offset: lump_offset >= 0
    valid_length: lump_size >= 0

end -- class WAD_DIRECTORY
