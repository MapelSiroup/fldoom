note
	description: "Summary description for {VEC3}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	VEC3

create
    make,
    make_vec3,
    make_from_vec

feature -- Attributes

    x: DOUBLE
    y: DOUBLE
    z: DOUBLE

feature -- Initialization

    make
        do
            x := 0.0
            y := 0.0
            z := 0.0
        end

    make_vec3 (a_x, a_y, a_z: DOUBLE)
        do
            x := a_x
            y := a_y
            z := a_z
        end

    make_from_vec (v: VEC3)
        do
            x := v.x
            y := v.y
            z := v.z
        end

feature -- Operations

    add (vec: VEC3)
        local
            nx, ny, nz: DOUBLE
        do
            nx := x + vec.x
            ny := y + vec.y
            nz := z + vec.z
            set_values (nx, ny, nz)
        end

    subtract (vec: VEC3)
        local
            nx, ny, nz: DOUBLE
        do
            nx := x - vec.x
            ny := y - vec.y
            nz := z - vec.z
            set_values (nx, ny, nz)
        end

    scale (s: DOUBLE)
        do
            set_values (x * s, y * s, z * s)
        end

    dot (vec: VEC3): DOUBLE
        do
            Result := x * vec.x + y * vec.y + z * vec.z
        end

    cross (vec: VEC3): VEC3
        local
            nx, ny, nz: DOUBLE
        do
            nx := y * vec.z - z * vec.y
            ny := z * vec.x - x * vec.z
            nz := x * vec.y - y * vec.x
            create result.make_vec3 (nx, ny, nz)
        end

    normalize
        local
            length, den: DOUBLE
        do
            length := get_length
            if length /= 0.0 then
                den := 1 / length
                x := x * den
                y := y * den
                z := z * den
            end
        end

feature -- Queries

    get_length: DOUBLE
        do
            Result := {DOUBLE_MATH}.sqrt (x * x + y * y + z * z)
        end

feature -- Output

    to_string: STRING
        do
            create Result.make_from_string ("VEC3{x=" + x.out + ", y=" + y.out + ", z=" + z.out + "}")
        end

feature -- Internal

    set_values (a_x, a_y, a_z: DOUBLE)
        do
            x := a_x
            y := a_y
            z := a_z
        end

end -- class VEC3
