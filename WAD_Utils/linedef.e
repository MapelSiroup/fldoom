note
	description: "Summary description for {LINEDEF}."
	author: "Florent Perreault"
	date: "1/18/2023"
	revision: "$Revision$"

class
	LINEDEF

create
    make

feature -- Initialization

    make (a_start_vertex, a_end_vertex, a_flags, a_line_type, a_sector_tag, a_front_sidedef, a_back_sidedef: NATURAL_16)
        do
            start_vertex := a_start_vertex
            end_vertex := a_end_vertex
            flags := a_flags
            line_type := a_line_type
            sector_tag := a_sector_tag
            --0xFFFF means not defined, sidedef doens't exist
            --if (linedef.RightSidedef == 0xFFFF)
            front_sidedef := a_front_sidedef
            back_sidedef := a_back_sidedef
        end

feature -- Access

    start_vertex: NATURAL_16
    end_vertex: NATURAL_16
    flags: NATURAL_16
    line_type: NATURAL_16
    sector_tag: NATURAL_16
    front_sidedef: NATURAL_16
    back_sidedef: NATURAL_16

end -- class LINEDEF
