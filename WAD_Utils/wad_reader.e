﻿note
	description: "Summary description for {WAD_READER}."
	author: "FLORENT PERREAULT"
	date: "1/19/2024"
	revision: "$Revision$"

class
	WAD_READER

inherit
	CONSOLE_LOGGER

--create
--    make

feature -- Constants

    BYTE_SIZE: INTEGER = 1
    SHORT_SIZE: INTEGER = 2
    INTEGER_SIZE: INTEGER = 4

--feature -- Initialization
--
--    make
--        -- Initialization if needed
--    do
--        -- Initialization code if needed
--
--    end

feature -- File Read Operations

    read_wad (a_file_path: STRING): ARRAYED_LIST [NATURAL_8]
   	local
   		current_progress,last_progress: STRING
        l_file: RAW_FILE
        l_data: ARRAYED_LIST [NATURAL_8]
        l_temp_data: ARRAYED_LIST [CHARACTER_8]
    do
        create l_file.make_with_name (a_file_path)
        if l_file.exists and l_file.is_readable then
            l_file.open_read
            if l_file.is_open_read then
            	create l_temp_data.make (l_file.count)	-- init CHAR_BUFFER:tm: (patent pending)
            	create l_data.make (0)					-- init BYTE_BUFFER:tm: (also patent pending :wink:)
				l_file.readstream (l_file.count)		-- read the entirety of the file as a single STRING_8 then stores it into its internal `last_string:STRING_8` variable.
--					log_info("l_file pointer data: "+l_file.file_pointer.out+", "+l_file.count.out)	-- if we want this information sometimes it can help and doesnt require to search in the debugger for it.

					-- Get the POINTER of the ARRAYED_LIST. then COPY ALL BYTES FROM `l_file`'s `file_pointer` into the array (type mismatch? CHARACTER_8 vs NATURAL_8) should be fine since its the same data right ? bit-wise i mean.
--																								-- oh yeah update on this comment: IT FUCKING WASNT; I SPENT THE WHOLE DAY FIGURING OUT HOW TO CONVERT THE TWO TYPES.
					l_temp_data.area.copy_data (l_file.last_string.out.area,0,0, l_file.count)
--					l_data.area.base_address.memory_copy (l_file.last_string.out.area.base_address, l_file.count)	-- crashes when returning result but still triggers print strings from caller function?
--					l_data.to_array (l_file.linear_representation)	--> could've used this but i've been working on the memcpy for like 3 days
--																  	--> straight so fuck it we just doing a very dirty pointer trick.
--																  	--> AFTER CONSIDERATION THIS MIGHT STILL BE VIABLE IF WE ARENT REACHING THE READ OPERATION SPEEDS I WANT

--				log_info("l_temp_data pointer data: "+l_temp_data.area.base_address.out+", "+l_temp_data.count.out)-- useful pointer information in case we want it.
                l_file.close
				create last_progress.make_from_string(".")
				create current_progress.make_empty
				print("%N") -- secures a new line to put the progress bar into
--				l_data.area.base_address.memory_copy (l_temp_data.area.base_address, l_temp_data.count)
                    across 1 |..| (l_temp_data.count) as la_index loop
                        l_data.extend(l_temp_data[la_index.item].item.natural_32_code.as_natural_8)	--> explanation below:
--                      -- so in order to make the arrayed_list of NATURAL_8 aka UINT8_t, we need to find the appropriate byte at the same index in the CHAR_BUFFER `l_temp_data`
--						-- we then take the item value of that array index, convert it to the ascii code corresponding to it and then we convert it back into a natural_8
--						-- the funny thing is that this could all be done in 1 step or 2 depending on the implementation by simply allowing either array copy from memcpy in the
--						-- `POINTER` class which we could either use as well for the byte "conversion" (big quotes on that one) of a CHAR* into a NATURAL_8 since down to the
--						-- hardware level they are fundamentally the same bit-wise. if you couldnt tell already, im starting to loose my mind and this was only one of the first steps.

                    --------- This section is flair only [updates every loop]
                    	current_progress := wad_reading_progress(l_data.count, l_temp_data.count).out
						if not last_progress.same_string(current_progress) and not last_progress.is_empty then
							ansi_control.move_cursor_up(1)
                    		ansi_control.erase_line
							log_load_crit(current_progress.out)
							last_progress := current_progress
						end



                    end
--                -------------------- DBG --------------------
--				-- this debug comment/uncomment bit is for data parity to display bytes as their ascii values(*not hex* however this could be a simple fix),
--				-- USED to compare with a hex dumper. i use `HxD` for this.
--				l_data.merge_right (l_temp_data)
--				log_info("l_data pointer data: "+l_data.area.base_address.out+", "+l_data.count.out)
--				-- change l_data.count if you dont want it to spam forever, i usually just care for a couple of lumps/directory info.
--                    across 1 |..| 8 as la_index loop
--                        print(l_data[la_index.item].out + " ")
--                    end
--				print("%N")
--                ---------------------------------------------
				log_succ_crit("m_wad_data BYTEBUFFER generated, LOADED FILE INTO RAM SUCCESSFULLY")
				result := l_data	--returns the copied file as UINT8_T arrayed_list or in eiffel format: NATURAL_8
            else
            	-- FILE EXISTS BUT FAILS TO OPEN
                log_err ("Failed to open file for binary reading%N")
                create result.make(0)
            end
        else
        	-- wrong path, you fucked up somewhere like 3 functions ago
            log_err("File does not exist or is not readable%N")
            create result.make(0)
        end
        if not attached l_data then	-- we dont need to do a else statement here because if the array is valid we already assigned result with the necessary data.
        	log_info_crit("read_binary_file failed, a 0'd array will return. if this message trigger, >find why<")
        	create result.make(0)
        end

    end	-- END OF `read_wad`

    wad_reading_progress(data_read_count, data_total_count:INTEGER): STRING
    local
    	i, left,chunks_filled,percentage: INTEGER
   		dots, empty: STRING
    do
    	-- init the progress bar
    	--create dots.make_empty
    	-- Calculate the percentage of filled chunks --
        if data_total_count > 0 then
            percentage := (data_read_count * 100) // data_total_count
        else
            percentage := 0
        end

    	-- Calculate how many 1/20th (5%) chunks should be filled
        chunks_filled := percentage // 5

        -- Optional: You can use chunks_filled to create a string representing the progress bar.
        --from
        --    i := 1
        --until
        --    i > chunks_filled
        --loop
        --    dots := dots + "#" -- Use a dot or another character to represent a filled chunk
        --    i := i + 1
        --end
        -- make empty spaces left for the progress unfilled chunks
    	create dots.make_filled ('#', chunks_filled)
		create empty.make_filled (' ', (20-chunks_filled).item)
        create result.make_from_string ("["+dots.out+empty.out+"] "+percentage.out+"%%")
    end

feature -- READ BYTES


    read_2_bytes (p_wad_data: ARRAYED_LIST [NATURAL_8]; offset: INTEGER): NATURAL_16
		local
        	byte1, byte2: NATURAL_8
       		read_value: NATURAL_16
    	do
       	 	if (offset + 1) <= p_wad_data.upper then	-- make sure we arent reading further than the bounds of the WAD_Data array
	        	byte1 := p_wad_data[offset]
	        	byte2 := p_wad_data[offset + 1]
	        else
	        	log_err("Trying to read bytes from beyond the data array bounds, rescued operation, continuing with 0'd value")
	        	byte1 := 0; byte2 := 0
       	 	end
        	read_value := byte1.to_natural_16.bit_or(byte2.to_natural_16 |<< 8)
--        	log_info("Read 2 bytes in BYTE_ORDER.LITTLE_ENDIAN of value:"+byte1.out+", "+byte2.out+" output: "+ read_value.to_hex_string)
        	Result := read_value
        end

    read_4_bytes (p_wad_data: ARRAYED_LIST [NATURAL_8]; offset: INTEGER): NATURAL_32
    local
        byte1, byte2, byte3, byte4: NATURAL_8
        read_value: NATURAL_32
    do
--    	log_info("eiffel-corrected OFFSET="+offset.out + " natural offset = 0x0"+(offset - 1).out+" TO 0x0"+(offset + 2).to_hex_character.out+" p_wad_data bytes= "+p_wad_data.count.out+" p_wad_upper="+p_wad_data.upper.out )
        if (offset + 4) <= p_wad_data.upper then	-- make sure we arent reading further than the bounds of the WAD_Data array, TODO: FIX THIS INTO A PROPER RESCUE POSTCONDITION
			byte1 := p_wad_data[offset]
            byte2 := p_wad_data[offset + 1]
            byte3 := p_wad_data[offset + 2]
            byte4 := p_wad_data[offset + 3]
	    else
	        log_err("Trying to read bytes from beyond the data array bounds, rescued operation, continuing with 0'd value")
	        byte1 := 0; byte2 := 0; byte3 := 0; byte4 := 0
        end

		read_value :=
    	byte1.to_natural_32.bit_or (
        	(byte2.to_natural_32.bit_shift_left (8)).bit_or (
            	(byte3.to_natural_32.bit_shift_left (16)).bit_or (
                	(byte4.to_natural_32.bit_shift_left (24))
        	    )
       		)
   		)
--        log_info("Raw bytes as HEX in BYTE_ORDER.LITTLE_ENDIAN output: "+read_value.to_hex_string)
        Result := read_value
    end

    read_n_bytes (p_wad_data: ARRAYED_LIST [NATURAL_8]; offset, n_bytes: INTEGER): ARRAYED_LIST[NATURAL_8]
    	-- READ `n_bytes` FROM `p_wad_data` AND MEMCPY from `offset` until `offset + BYTE_SIZE * n_bytes` INTO `Result` using `read_value`.
    	local
       		read_value: ARRAYED_LIST[NATURAL_8]
    	do
			create read_value.make (0)
        	if (offset + BYTE_SIZE * n_bytes) <= p_wad_data.upper then	-- make sure we arent reading further than the bounds of the WAD_Data array, TODO: FIX THIS INTO A PROPER RESCUE POSTCONDITION
				read_value.area.copy_data (p_wad_data.area, offset, 0, n_bytes)
	    	else
	        	log_err("Trying to read bytes from beyond the data array bounds, rescued operation, continuing with 0'd value")
        	end
        Result := read_value
    end

feature -- READ HEADER AND LUMPS
    read_header_data (p_wad_data: ARRAYED_LIST [NATURAL_8]; offset: INTEGER; header: HEADER)
    	-- reads the WAD header data and fill the header class with the data we find
        do
            -- 0x00 to 0x03
            header.wad_type := read_natural_32_as_string(read_4_bytes(p_wad_data, offset))
			log_succ("HEADER WAD TYPE RETRIEVED: "+header.wad_type.out)

            -- 0x04 to 0x07
            header.directory_count := read_4_bytes (p_wad_data, offset + 4)--.to_integer_32.to_natural_32 --.bit_and (0xFF000000) -- silly me forgot i had to do `&` operations and should return a long but meh imma keep it natural_32 what could go wrong?
			log_succ("HEADER DIRECTORY COUNT RETRIEVED: "+header.directory_count.out)
            -- 0x08 to 0x0b
            header.directory_offset := read_4_bytes (p_wad_data, offset + 8)--.to_integer_64.bit_and (0xFFFFFFFF).to_natural_32
			log_succ("HEADER DIRECTORY OFFSET RETRIEVED: "+header.directory_offset.out)

            log_succ_crit("Successfully read WAD file's HEADER of type : "+header.wad_type.out+" containing: "+header.directory_count.out+" lumps.")	-- shouldnt display directory offset not needed for now " at offset: "+header.directory_offset.out+
        end

    read_directory_data (p_wad_data: ARRAYED_LIST [NATURAL_8]; offset: INTEGER): WAD_DIRECTORY
        local
        	l_lump: WAD_DIRECTORY
        do
        	create l_lump.make_empty
            if offset < p_wad_data.count then
			--	offset + 0x0F <= byte_buffer's byte count

	            -- 0x00 to 0x03
	            l_lump.lump_offset := read_4_bytes (p_wad_data, offset + 1)

	            -- 0x04 to 0x07
	            l_lump.lump_size := read_4_bytes (p_wad_data, offset + 5)	-- man im gonna- im gonna break my monitor i swear
--				log_succ("LUMP SIZE RETRIEVED: "+l_lump.lump_size.out)		-- UPDATE 1/26/2024: I WAS OFF BY 1 BYTE GOD FUCKING DAMMIT IM GONNA FUCKING LOOSE IT ALL AAAAAA FUCKING EIFFEL `moment` WHERE THE INDEXES START AT 1; SHIFT ALL BY 1 !!
--	            -- 0x08 to 0x0F												-- I WAS STUCK ON THIS FOR 3 DAYS STRAIGHT JUST BECAUSE THE INDEX WAS OFF BY 1 byte.

				l_lump.lump_name.wipe_out
				l_lump.lump_name.extend (p_wad_data[offset + 9].to_character_8)
    			l_lump.lump_name.extend (p_wad_data[offset + 10].to_character_8)
				l_lump.lump_name.extend (p_wad_data[offset + 11].to_character_8)
				l_lump.lump_name.extend (p_wad_data[offset + 12].to_character_8)
				l_lump.lump_name.extend (p_wad_data[offset + 13].to_character_8)
				l_lump.lump_name.extend (p_wad_data[offset + 14].to_character_8)
				l_lump.lump_name.extend (p_wad_data[offset + 15].to_character_8)
				l_lump.lump_name.extend (p_wad_data[offset + 16].to_character_8)
--				directory.lump_name.put ('\', 8)	-- no need for null termination in eiffel ? (big maybe)
--	            log_info("Successfully read directory data of lump: "+l_lump.lump_name.out+" at offset: "+l_lump.lump_offset.out+" of size: "+l_lump.lump_size.out)
	        	result := l_lump
	        else
	        	if not (offset = p_wad_data.count) then
	        		log_err_crit("Out of indices for array length: "+p_wad_data.count.out+" index: "+offset.out)
            		create result.make_empty
	        	else
	        		create result.make_empty
	        	end
            end
        end

        read_vertex_data (p_wad_data: ARRAYED_LIST [NATURAL_8]; offset: INTEGER): POINT
    	-- reads the MAP vertex data and returns a point class with the data we find
    	local
    		vertex: POINT
        do
        	create vertex.make
        	-- 0x00 to 0x01
        	vertex.x := read_natural_16_as_int (read_2_bytes(p_wad_data, offset + 1))
        	-- 0x02 to 0x03
        	vertex.y := read_natural_16_as_int (read_2_bytes(p_wad_data, offset + 3))
            result := vertex
        end

        read_linedef_data (p_wad_data: ARRAYED_LIST [NATURAL_8]; offset: INTEGER): LINEDEF
    	-- reads the MAP linedefs data and return a linedef class with the data we find
    	local
    		start_vertex, end_vertex, flags, line_type, sector_type, r_sidedef, l_sidedef: NATURAL_16
--    		l_linedef: LINEDEF
        do
        	-- Read 7*2 bytes
        	start_vertex:= read_2_bytes(p_wad_data, offset + 1)	-- 0x00 to 0x01
--        	print (start_vertex.out+" ")
        	end_vertex 	:= read_2_bytes(p_wad_data, offset + 3)	-- 0x02 to 0x03
--        	print (end_vertex.out+" ")
        	flags 		:= read_2_bytes(p_wad_data, offset + 5)	-- 0x04 to 0x05
    		line_type 	:= read_2_bytes(p_wad_data, offset + 7)	-- 0x06 to 0x07
    		sector_type := read_2_bytes(p_wad_data, offset + 9)	-- 0x08 to 0x09
    		r_sidedef 	:= read_2_bytes(p_wad_data, offset + 11)-- 0x0A to 0x0B
--    		print (r_sidedef.out+" ")
    		l_sidedef 	:= read_2_bytes(p_wad_data, offset + 13)-- 0x0C to 0x0D
--    		print (l_sidedef.out+"%N")

--			l_linedef.make(start_vertex, end_vertex, flags, line_type, sector_type, r_sidedef, l_sidedef)
            create result.make (start_vertex, end_vertex, flags, line_type, sector_type, r_sidedef, l_sidedef)
        end

		read_thing_data (p_wad_data: ARRAYED_LIST [NATURAL_8]; offset: INTEGER): THING
    	-- reads the MAP things data and returns a thing with the data we find
    	local
    		l_posx, l_posy: INTEGER_16
    		l_angle, l_type, l_flags: NATURAL_16
        do
        	-- SIGNED 16 BIT INTEGERS
        	l_posx := read_natural_16_as_int (read_2_bytes(p_wad_data, offset + 1))-- 0x00 to 0x01
        	l_posy := read_natural_16_as_int (read_2_bytes(p_wad_data, offset + 3))-- 0x02 to 0x03
        	-- NATURALS
        	l_angle:= read_2_bytes(p_wad_data, offset + 5)	-- 0x04 to 0x05
    		l_type := read_2_bytes(p_wad_data, offset + 7)	-- 0x06 to 0x07
    		l_flags:= read_2_bytes(p_wad_data, offset + 9)	-- 0x08 to 0x09
            create result.make(l_posx, l_posy, l_angle, l_type, l_flags)
        end

        read_node_data (p_wad_data: ARRAYED_LIST [NATURAL_8]; offset: INTEGER): NODE
        -- reads the MAP nodes data and returns a node with the data we find
    	local
    		l_x_partition, l_y_partition, l_change_x_partition, l_change_y_partition,
            l_right_box_top, l_right_box_bottom, l_right_box_left, l_right_box_right,
            l_left_box_top, l_left_box_bottom, l_left_box_left, l_left_box_right:INTEGER_16
    		l_right_child_id, l_left_child_id:NATURAL_16
        do
        	-- Read 14*2 bytes
			-- INTEGERS
			l_x_partition := read_natural_16_as_int (read_2_bytes(p_wad_data, offset + 1));	-- 0x00 to 0x01
    		l_y_partition := read_natural_16_as_int (read_2_bytes(p_wad_data, offset + 3));	-- 0x02 to 0x03
    		l_change_x_partition := read_natural_16_as_int (read_2_bytes(p_wad_data, offset + 5));	-- 0x04 to 0x05
    		l_change_y_partition := read_natural_16_as_int (read_2_bytes(p_wad_data, offset + 7));	-- 0x06 to 0x07
    		l_right_box_top := read_natural_16_as_int (read_2_bytes(p_wad_data, offset + 9));	-- 0x08 to 0x09
    		l_right_box_bottom := read_natural_16_as_int (read_2_bytes(p_wad_data, offset + 11));-- 0x0A to 0x0B
    		l_right_box_left := read_natural_16_as_int (read_2_bytes(p_wad_data, offset + 13));	-- 0x0C to 0x0D
    		l_right_box_right := read_natural_16_as_int (read_2_bytes(p_wad_data, offset + 15)); -- 0x0E to 0x0F
    		l_left_box_top := read_natural_16_as_int (read_2_bytes(p_wad_data, offset + 17)); -- 0x10 to 0x11
    		l_left_box_bottom := read_natural_16_as_int (read_2_bytes(p_wad_data, offset + 19));-- 0x12 to 0x13
    		l_left_box_left := read_natural_16_as_int (read_2_bytes(p_wad_data, offset + 21));-- 0x14 to 0x15
    		l_left_box_right := read_natural_16_as_int (read_2_bytes(p_wad_data, offset + 23));-- 0x16 to 0x17
			-- NATURALS
    		l_right_child_id := read_2_bytes(p_wad_data, offset + 25);-- 0x18 to 0x19
    		l_left_child_id := read_2_bytes(p_wad_data, offset + 27);-- 0x1A to 0x1B

            create result.make (l_x_partition, l_y_partition, l_change_x_partition, l_change_y_partition,
          						l_right_box_top, l_right_box_bottom, l_right_box_left, l_right_box_right,
          						l_left_box_top, l_left_box_bottom, l_left_box_left, l_left_box_right,
								l_right_child_id, l_left_child_id)
        end

		read_subsector_data(p_wad_data: ARRAYED_LIST [NATURAL_8]; offset: INTEGER): SUBSECTOR
		local
			l_seg_count, l_first_seg_id: NATURAL_16
		do
			-- Read 2*2 bytes
			l_seg_count := read_2_bytes(p_wad_data, offset + 1)
			l_first_seg_id := read_2_bytes(p_wad_data, offset + 3)
			create result.make (l_seg_count, l_first_seg_id)
		end

		read_seg_data (p_wad_data: ARRAYED_LIST [NATURAL_8]; offset: INTEGER): SEG
		local
			l_start_vertex_id, l_end_vertex_id, l_angle, l_linedef_id, l_direction, l_offset: NATURAL_16
		do
			-- Read 6*2 bytes
			l_start_vertex_id := read_2_bytes(p_wad_data, offset + 1)
			l_end_vertex_id := read_2_bytes(p_wad_data, offset + 3)
			l_angle := read_2_bytes(p_wad_data, offset + 5)
			l_linedef_id := read_2_bytes(p_wad_data, offset + 7)
			l_direction := read_2_bytes(p_wad_data, offset + 9)
			l_offset := read_2_bytes(p_wad_data, offset + 11)
			create result.make (l_start_vertex_id, l_end_vertex_id, l_angle, l_linedef_id, l_direction, l_offset)
		end

feature -- handy dandies

	read_natural_32_as_string (value: NATURAL_32): STRING_8
   		-- Read a NATURAL_32 as a 4 CHARACTER_8 string.
    	local
    		byte1, byte2, byte3, byte4: NATURAL_8
        	str: STRING_8
    	do
        	-- Convert each byte individually and concatenate them.
        	-- Extract individual bytes using bitwise operations.
        	byte1 := (value |>> 24).to_natural_8
        	byte2 := (value |>> 16).to_natural_8
        	byte3 := (value |>> 8).to_natural_8
        	byte4 := (value.bit_and (0x000000FF)).to_natural_8

			-- Initialize an empty string
    		create str.make_empty

        	str := byte4.to_character_8.out + byte3.to_character_8.out + byte2.to_character_8.out + byte1.to_character_8.out

        	-- OLD -- str := value.item(1).to_character_8 + value.item (2).to_character_8 + value.item (3).to_character_8 + value.item (4).to_character_8


        	-- Now, `str` contains the 4 CHARACTER_8 string representation of the NATURAL_32.
        	Result := str
    	end

	read_natural_32_as_int (value: NATURAL_32): INTEGER
	    -- Read a NATURAL_32 as a LITTLE_ENDIAN NUMERIC `INTEGER` VALUE
	    local
	        byte1, byte2, byte3, byte4: NATURAL_8
	        int_value: INTEGER
	    do
	        -- Extract individual bytes using bitwise operations.
	        byte1 := (value.bit_and(0xFF000000) |>> 24).to_natural_8
	        byte2 := (value.bit_and(0x00FF0000) |>> 16).to_natural_8
	        byte3 := (value.bit_and(0x0000FF00) |>>  8).to_natural_8
	        byte4 := (value.bit_and(0x000000FF)       ).to_natural_8
	        -- Interpret the bytes using bit manipulation
	        int_value := ((byte1.to_integer_32) + (byte2.to_integer_32 |<< 8) + (byte3.to_integer_32 |<< 16) + (byte4.to_integer_32 |<< 24)) --OLD--(byte4.to_integer_32 |<< 24) + (byte3.to_integer_32 |<< 16) + (byte2.to_integer_32 |<< 8) + byte1.to_integer_32
--	        int_value := byte4.bit_or((byte3 |<< 8).bit_or((byte2 |<< 16).bit_or((byte1 |<< 24)))).to_integer_32
	        -- Now, `int_value` contains the 4 bytes as representation of the NATURAL_32 into INTEGER_32.
--	        log_warn(int_value.out+" from bytes (1-4): "+byte1.to_integer_32.out+","+(byte2.to_integer_32 |<< 8).out+","+(byte3.to_integer_32 |<< 16).out+","+(byte4.to_integer_32 |<< 24).out)
--	        log_warn(value.out+" from bytes (4-1): "+byte4.to_integer_32.out+","+(byte3.to_integer_32 |<< 8).out+","+(byte2.to_integer_32 |<< 16).out+","+(byte1.to_integer_32 |<< 24).out)
	        Result := int_value
	    end

	    read_natural_16_as_int (value: NATURAL_32): INTEGER_16
	    -- Read a NATURAL_16 as a LITTLE_ENDIAN NUMERIC `INTEGER` VALUE
	    local
	        byte1, byte2: NATURAL_8
	        int_value: INTEGER_16
	    do
	        -- Extract individual bytes using bitwise operations.
	        byte1 := (value |>>  8).to_natural_8
	        byte2 := (value).to_natural_8

	        -- Interpret the bytes using bit manipulation
	        int_value := byte2.to_integer_16.bit_or((byte1.to_integer_16 |<< 8))
--	        print(byte1.to_hex_string+byte2.to_hex_string)
	        -- Now, `int_value` contains the 2 bytes as representation of the NATURAL_16 into INTEGER_16.
	        Result := int_value
	    end

end -- class WAD_READER
