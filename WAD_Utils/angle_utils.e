note
	description: "ANGLE Utilities for computing normalization and remainders of vector angle math"
	author: "Florent Perreault"
	date: "3/31/2024"
	revision: "1.0.0"

class
	ANGLE_UTILS

feature -- Access
	fmod (x, div: REAL_64): REAL_64
    local
        int_part: INTEGER
    do
        -- if div or x is nan, return x directly
        if div.is_nan or else x.is_nan then
			Result := x
        elseif div = 0.0 then
            Result := {REAL_64}.nan -- Return NAN if div is 0
        else
            int_part := (x.truncated_to_integer // div.truncated_to_integer) -- Integer division
            Result := x - int_part * div -- Compute the remainder
        end
        ensure				-- sneaky eiffel thingy because without it it causes a `VUNO` error
        instance_free: class-- non-object call on non-class feature
    end

	normalize_degrees(a_angle: REAL_64): REAL_64
	local
		l_angle: REAL_64
	do
		l_angle := fmod(a_angle, 360)	-- MODULO DOESNT WORK ON FLOATING POINT VALUES IN EIFFEL
		if l_angle < 0 then
			l_angle := a_angle + 360
		end
		result := l_angle
		ensure				--Very useful for utility classes**
        instance_free: class--https://stackoverflow.com/questions/52778337/eiffel-how-do-i-create-and-use-an-util-class-or-call-static-method-from-class
	end

	normalize_degrees_from_rad(a_radians: REAL_64): REAL_64
	local
		l_angle: REAL_64
	do
		-- CONVERT TO DEGREES
		l_angle := a_radians * (180 / ({MATH_CONST}.PI))
		-- NORMALIZE DEGREES
		l_angle := fmod(l_angle, 360)
		if l_angle < 0 then
			l_angle := l_angle + 360
		end
		result := l_angle
		ensure
        instance_free: class
	end

	to_rad(a_radians: REAL_64): REAL_64
	local
		--l_angle: REAL_64
	do
		-- CONVERT TO DEGREES
		result :=	(a_radians.product(180)).quotient({MATH_CONST}.PI)
		-- := l_angle
		ensure
        instance_free: class
	end
end
