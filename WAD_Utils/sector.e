note
	description: "Summary description for {SECTOR}."
	author: "Florent Perreault"
	date: "4/18/2024"
	revision: "$Revision$"

class
	SECTOR

create
    make

feature -- Constants
    TAN_45_DEG: REAL_64
--    CANVAS_HEIGHT_INV: REAL_64 = 1.0 / 800.0

feature -- Attributes
    floor_height, ceiling_height: INTEGER_16
    floor_texture_name, ceiling_texture_name: STRING
    light_level, special_type, tag_number: INTEGER_16
    color: GAME_COLOR
    --floor_flat, ceiling_flat: FLAT
    --ceiling_pic: PICTURE
    --random: ARRAY [BOOLEAN]

feature -- Initialization
    make (a_floor_height, a_ceiling_height: INTEGER_16;
          a_floor_texture_name, a_ceiling_texture_name: STRING;
          a_light_level, a_special_type, a_tag_number: INTEGER_16)
          --a_floor_flat, a_ceiling_flat: FLAT;
          --a_ceiling_pic: PICTURE
        local
        	l_random: RANDOM
        do
            floor_height := a_floor_height
            ceiling_height := a_ceiling_height
            floor_texture_name := a_floor_texture_name
            ceiling_texture_name := a_ceiling_texture_name
            light_level := a_light_level
            special_type := a_special_type
            tag_number := a_tag_number
            -- random color
            create l_random.make
            create color.make_rgb ((l_random.i_th(1) \\ 255 + 100).to_natural_8,	-- r
            					   (l_random.i_th(2) \\ 255 + 100).to_natural_8,	-- g
             					   (l_random.i_th(3) \\ 255 + 100).to_natural_8)	-- b
            --floor_flat := a_floor_flat
            --ceiling_flat := a_ceiling_flat
            --ceiling_pic := a_ceiling_pic
            TAN_45_DEG := {DOUBLE_MATH}.tangent({DOUBLE_MATH}.PI / {ANGLE_UTILS}.to_rad(45.0))
        end
--invariant
--    floor_height_not_void: floor_height /= Void
--    ceiling_height_not_void: ceiling_height /= Void
--    floor_texture_name_not_void: floor_texture_name /= Void
--    ceiling_texture_name_not_void: ceiling_texture_name /= Void
--    light_level_not_void: light_level /= Void
--    special_type_not_void: special_type /= Void
--    tag_number_not_void: tag_number /= Void
--    color_not_void: color /= Void
--    floor_flat_not_void: floor_flat /= Void
--    ceiling_flat_not_void: ceiling_flat /= Void
--    ceiling_pic_not_void: ceiling_pic /= Void

end -- class SECTOR

