note
	description: "Class defining a segment"
	author: "Florent Perreault"
	date: "2/29/2024"
	revision: "1.0.0"

class
	SEG

create
    make

feature -- Attributes

    start_vertex_id: NATURAL_16
    end_vertex_id: NATURAL_16
    angle: NATURAL_16
    linedef_id: NATURAL_16
    direction: NATURAL_16
    offset: NATURAL_16
    --right_sector: SECTOR
    --left_sector: SECTOR

feature -- Initialization

    make (a_start_vertex_id, a_end_vertex_id, a_angle, a_linedef_id, a_direction, a_offset : NATURAL_16)--,a_right_sector,a_left_sector)
        do
            start_vertex_id := a_start_vertex_id
    		end_vertex_id := a_end_vertex_id
    		angle := a_angle
    		linedef_id := a_linedef_id
    		direction := a_direction
    		offset := a_offset
            --right_sector := a_right_sector
            --left_sector := a_left_sector
        end

end
