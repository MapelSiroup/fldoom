note
	description: "Summary description for {PLAYER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	PLAYER
create
	make

feature -- Attributes

	m_id: INTEGER_16
    m_position_x: INTEGER_16 assign set_position_x
    m_position_y: INTEGER_16 assign set_position_y
	m_mouvement_wishdir: VEC2
	m_DIAG_MOVE_ERR: REAL_64
    m_angle: REAL_64 assign set_angle
    m_fov: REAL_32 assign set_fov
	m_turnrate: REAL_32 assign set_turnrate
	m_moverate: REAL_32 assign set_moverate
	b_turnright: BOOLEAN assign set_turnright
	b_turnleft: BOOLEAN assign set_turnleft
	b_moveforward: BOOLEAN assign set_moveforward
	b_movebackward: BOOLEAN assign set_movebackward
	b_straferight: BOOLEAN assign set_strafeleft
	b_strafeleft: BOOLEAN assign set_straferight

feature -- initialisation
	make(a_id: INTEGER_16)
	do
		m_id := a_id
		m_position_x := 1056
		m_position_y := -3616
		m_angle := 90
		m_fov := 90
		m_turnrate := 12
		m_moverate := 50
		create m_mouvement_wishdir.make
		m_DIAG_MOVE_ERR := 1/{DOUBLE_MATH}.sqrt(2)
		init_vec
	end

	init_vec
	local
		test_vec: VEC2
	do
	-- function to test rotation see if theres any issues
		create test_vec.make_vec2(1.98,0)
		test_vec.rotate_degrees(90)
		print("TEST 1: (" + test_vec.x.out +","+ test_vec.y.out +") | EXPECTED : (0,1)%N")
		test_vec.set_values(-2.5,-1)
		test_vec.rotate_degrees(-90)
		print("TEST 2: (" + test_vec.x.out +","+ test_vec.y.out +") | EXPECTED : (-1,2)%N")
		test_vec.set_values(-.0005,-1)
		test_vec.rotate_degrees(-90)
		print("TEST 2: (" + test_vec.x.out +","+ test_vec.y.out +") | EXPECTED : (-1,.0005)%N")
	end
feature -- Setters
	set_position_x(a_posx:INTEGER_16)
	do

	end

	set_position_y(a_posy:INTEGER_16)
	do

	end

	set_angle(a_angle:REAL_64)
	do

	end
	set_fov(a_fov:REAL_32)
	do

	end
	set_turnrate(a_value:REAL_32)
	do

	end
	set_moverate(a_value:REAL_32)
	do

	end
	set_turnright(a_value:BOOLEAN)
	do
		b_turnright := a_value
		ensure
		is_assign: b_turnright = a_value
	end
	set_turnleft(a_value:BOOLEAN)
	do
		b_turnleft := a_value
		ensure
		is_assign: b_turnleft = a_value
	end
	set_moveforward(a_value:BOOLEAN)
	do
		b_moveforward := a_value
		ensure
		is_assign: b_moveforward = a_value
	end
	set_movebackward(a_value:BOOLEAN)
	do
		b_movebackward := a_value
		ensure
		is_assign: b_movebackward = a_value
	end
	set_straferight(a_value:BOOLEAN)
	do
		b_straferight := a_value
		ensure
		is_assign: b_straferight = a_value
	end
	set_strafeleft(a_value:BOOLEAN)
	do
		b_strafeleft := a_value
		ensure
		is_assign: b_strafeleft = a_value
	end

feature -- MOVEMENT CODE
	-- we need to move in relation to the angle direction, m_angle is always the player's forward vector
	mouvement_update(a_timestamp, old_timestamp: NATURAL_32)
	local
		delta_time: REAL_64
	do
		-- calculate delta
		delta_time := (a_timestamp - old_timestamp) * 0.001 -- roughly divide into a ms timeframe, in theory doing it with a multiplication should be faster than a division
		if b_turnright and not b_turnleft then
			turn_right
		end
		if b_turnleft and not b_turnright then
			turn_left
		end

		m_mouvement_wishdir.set_values(0,0)	-- reset mouvement wished direction vector before calculating next move step
		if b_moveforward then
			move_forward
		end
		if b_movebackward then
			move_backward
		end
		if b_strafeleft then
			strafe_left
		end
		if b_straferight then
			strafe_right
		end

		-- little movement fix, when we are doing a diagonal movement in our next step we need to clamp its value to not make unintended speed boost.
		if m_mouvement_wishdir.x /= 0 and m_mouvement_wishdir.y /= 0 then
			m_mouvement_wishdir.scale(m_DIAG_MOVE_ERR)
		--	print("Held keys: "+ b_moveforward.out +","+ b_strafeleft.out +","+ b_movebackward.out +","+ b_straferight.out)
		end

		-- add the wished direction vector to our split "vector" using m_angle as orientation
		m_mouvement_wishdir.rotate_degrees(m_angle)
		m_position_x := (m_position_x + m_mouvement_wishdir.x.truncated_to_integer.to_integer_16)
		m_position_y := (m_position_y + m_mouvement_wishdir.y.truncated_to_integer.to_integer_16)
	end

    turn_left	-- rotate camera to the left
    do
        m_angle := {ANGLE_UTILS}.normalize_degrees(m_angle + (0.1875 * m_turnrate))
    end

    turn_right	-- rotate camera to the right
    do
        m_angle := {ANGLE_UTILS}.normalize_degrees(m_angle - (0.1875 * m_turnrate))
    end

	move_forward--(dt: REAL_64)
	do
		m_mouvement_wishdir.set_y(m_mouvement_wishdir.y + (m_moverate))
	end
	move_backward
	do
		m_mouvement_wishdir.set_y(m_mouvement_wishdir.y - (m_moverate))
	end
	strafe_left
	do
		m_mouvement_wishdir.set_x(m_mouvement_wishdir.x - (m_moverate))
	end
	strafe_right
	do
		m_mouvement_wishdir.set_x(m_mouvement_wishdir.x + (m_moverate))
	end


feature {NONE} -- FOV MATH
	clip_vertexes_in_fov (a_vert1, a_vert2: POINT): BOOLEAN	-- might not need `; v1_angle, v2_angle: REAL_64` v1 and v2 from a outbound source
    local
        v1_angle, v2_angle, v1_to_v2_span, half_fov, v1_moved, v2_moved, v1_moved_angle: REAL_64
    do
        v1_angle := angle_to_vertex (a_vert1)
        v2_angle := angle_to_vertex (a_vert2)

        v1_to_v2_span := v1_angle - v2_angle

        if v1_to_v2_span >= 180.0 then
            Result := False
        else
			-- ROTATE EVERYTHING
            v1_angle := v1_angle - m_angle
            v2_angle := v2_angle - m_angle

            half_fov := m_fov / 2.0
			-- validate and clip vertex 1 and shift it to fit inside our FOV
            v1_moved := v1_angle + half_fov

            if v1_moved > m_fov then
				-- now we know that V1, is outside the left side of the FOV
        		-- But we need to check if also V2 is outside.
        		-- Let's find out what is the size of the angle outside the FOV
                v1_moved_angle := v1_moved - m_fov

                if v1_moved_angle >= v1_to_v2_span then	-- Are both V1 and V2 outside?
                    Result := False
                else
					-- At this point V2 or part of the line should be in the FOV.
        			-- We need to clip the V1
                    v1_angle := half_fov
                end
            end
			-- Validate and Clip V2
            v2_moved := half_fov - v2_angle

            if v2_moved > m_fov then	-- Is V2 outside the FOV?
                v2_angle := -half_fov
            end

            v1_angle := v1_angle + 90.0
            v2_angle := v2_angle + 90.0

            Result := True
        end
    end

	angle_to_vertex(a_vert: POINT): REAL_64
	local
		Vdx, Vdy: REAL_64
	do
		Vdx := a_vert.X - m_position_x;
    	Vdy := a_vert.Y - m_Position_y;

		result := ({DOUBLE_MATH}.arc_tangent(Vdy.quotient(Vdx)) * 180) / {MATH_CONST}.PI
	end
end
