note
	description: ""
	author: "Florent Perreault"
	date: "1/30/2024"
	revision: "1.0.0"

class
	MAP

create
	make

feature	-- initialisation

	make(a_name: STRING_8; a_player: PLAYER)
	do
		m_name := a_name
		m_player := a_player
		create m_vertexes.make(0)
		create m_linedefs.make(0)
		create m_things.make(0)
		create m_nodes.make(0)
		create m_ssects.make(0)
		create m_segs.make(0)
		m_automap_scale_factor := 1
		m_lump_index := -1	-- Why ? i dont remember why i need to set it to a invalid value at first but i'll leave this here because past me knew what he was doing.
	end

feature -- Attributes

	m_name: 	STRING_8
	m_player: 	PLAYER
    m_vertexes: ARRAYED_LIST[POINT]
    m_linedefs: ARRAYED_LIST[LINEDEF]
    m_things: 	ARRAYED_LIST[THING]
    m_nodes: 	ARRAYED_LIST[NODE]
	m_ssects: 	ARRAYED_LIST[SUBSECTOR]
	m_segs: 	ARRAYED_LIST[SEG]
	m_xmin:		INTEGER_16
	m_xmax: 	INTEGER_16
	m_ymin: 	INTEGER_16
	m_ymax: 	INTEGER_16

	m_automap_scale_factor: INTEGER_16
	m_lump_index: INTEGER_16 assign set_lump_index

feature -- Setters

	set_lump_index(a_index:INTEGER_16)
	do
		m_lump_index := a_index
	ensure
		is_assign: m_lump_index = a_index
	end

feature -- append methods

	add_vertex(a_vertex: POINT)
	do
		m_vertexes.extend (a_vertex)

		-- xmin/xmax
		if m_xmin > a_vertex.x then
			m_xmin := a_vertex.x
		elseif m_xmax < a_vertex.x then
			m_xmax := a_vertex.x
		end

		-- ymin/ymax
		if m_ymin > a_vertex.y then
			m_ymin := a_vertex.y
		elseif m_ymax < a_vertex.y then
			m_ymax := a_vertex.y
		end

	end
	add_linedef(a_linedef: LINEDEF)
	do
		m_linedefs.extend (a_linedef)
	end

	add_thing(a_thing: THING)
	do
		if a_thing.type.to_integer_16 = m_player.m_id then	-- not sure if working
			m_player.m_position_x := a_thing.position_x.to_integer_16
			m_player.m_position_y := a_thing.position_y.to_integer_16
			m_player.m_angle := a_thing.angle.to_integer_16
		end

		m_things.extend (a_thing)
	end

	add_node(a_node: NODE)
	do
		m_nodes.extend (a_node)
	end

	add_ssector(a_subsector: SUBSECTOR)
	do
		m_ssects.extend (a_subsector)
	end

	add_segment(a_seg: SEG)
	do
		m_segs.extend (a_seg)
	end

feature -- IMPLEMENTATION


	is_point_on_left_side(a_x_pos,a_y_pos,a_node_id:INTEGER_16):BOOLEAN
	local
		dx,dy:INTEGER_16
	do
		dx := a_x_pos - m_nodes[a_node_id].x_partition
		dy := a_y_pos - m_nodes[a_node_id].y_partition
		result:= (((dx * m_nodes[a_node_id].change_y_partition) - (dy * m_nodes[a_node_id].change_x_partition)) <= 0)
	end

	remap_vx(a_vx:INTEGER_16;a_width:INTEGER_32): INTEGER_16
		-- remaps the given vertex `x` value to a viewport scaled version with padding as clamp
		local
			clamp_size,width_clamped:INTEGER_32
		do
			-- INIT VARS
			clamp_size := 30	-- clamp size in pixel (30 pixel from window border)
			width_clamped := a_width - clamp_size
			-- PREPARE YOURSELF FOR A MOUTHFUL
			result := ((m_xmin.max(a_vx.min(m_xmax)) - m_xmin) *
					  (width_clamped - clamp_size) // (m_xmax - m_xmin) + clamp_size).to_integer_16	-- xmax :D
		end

	remap_vy(a_vy:INTEGER_16;a_height:INTEGER_32): INTEGER_16
		-- remaps the given vertex `y` value to a viewport scaled version with padding as clamp
		local
			clamp_size,height_clamped:INTEGER_32
		do
			-- INIT VARS
			clamp_size := 30	-- clamp size in pixel (30 pixel from window border)
			height_clamped := a_height - clamp_size
			-- PREPARE YOURSELF FOR A MOUTHFUL
			result := (a_height - (m_ymin.max(a_vy.min(m_ymax)) - m_ymin) *
					  (height_clamped - clamp_size) // (m_ymax - m_ymin) - clamp_size).to_integer_16
		end

	render_automap(a_renderer: GAME_RENDERER)
	local
		l_xshift, l_yshift, i: INTEGER_16
		width, height: INTEGER_32
		v_end, v_start: POINT
		l_automap_color: GAME_COLOR
--		l_random: RANDOM
	do
--		l_xshift := m_xmin.abs	-- invert negative values (i feel like this would be better used with abs()
--								-- but they simply negate the value soo i dont know im just following the source code here)
--		l_yshift := m_ymin.abs	-- ?? idk if abs would be better than to revert the sign but oh well
		-- set custom color/sdl args before the loop
		create l_automap_color.make (255, 255, 255, 255)	-- white
		a_renderer.set_drawing_color (l_automap_color)
--		create l_random.make
		width  := a_renderer.viewport.integer_32_item(3)	-- get viewport width
		height := a_renderer.viewport.integer_32_item(4)	-- get viewport height
		from
			i:= 1
		until
			i > (m_linedefs.count-1)
		loop
			--------- coloring -------------
--			l_automap_color.set_red ((l_random.item \\ 255 + 100).to_natural_8)
----			print(l_automap_color.red.out+" ")
--			l_random.forth
--			l_automap_color.set_green ((l_random.item \\ 255 + 100).to_natural_8)
----			print(l_automap_color.green.out+" ")
--			l_random.forth
--			l_automap_color.set_blue ((l_random.item \\ 255 + 100).to_natural_8)
----			print(l_automap_color.blue.out+"%N")
--			l_random.forth
			--------------------------------
		a_renderer.set_drawing_color (l_automap_color)
		-- vertexes
		v_start := m_vertexes[m_linedefs[i.item].start_vertex.item+1]	-- also dont forget about the `+1` because of `eiffel moment`
		v_end 	:= m_vertexes[m_linedefs[i.item].end_vertex.item+1]		-- here as well

		-- rendering linedefs
		a_renderer.draw_line((remap_vx(v_start.x, width) // m_automap_scale_factor ),		-- remember to be careful around divisions that could have 0'd value
							 ((remap_vy(v_start.y, height) // m_automap_scale_factor) - 100),		-- cardinal to viewport manipulation
							 (remap_vx(v_end.x, width) // m_automap_scale_factor),
							 (remap_vy(v_end.y, height) // m_automap_scale_factor) - 100)
		i := i+1
		end	-- end loop
		render_automap_player(a_renderer)
		render_automap_node(a_renderer, m_nodes.count)
	end	-- end render_automap

	render_automap_player(a_renderer: GAME_RENDERER)
	local
		x,y,ray_x,ray_y: INTEGER_16
		--: REAL_64
		ray_vec: VEC2
		ray_length: INTEGER_16
		width, height: INTEGER_32
		l_automap_color: GAME_COLOR
	do
		-- set custom color/sdl args before the loop
		create l_automap_color.make (255, 0, 0, 255)	-- red
		a_renderer.set_drawing_color (l_automap_color)
		width  := a_renderer.viewport.integer_32_item(3)	-- get viewport width
		height := a_renderer.viewport.integer_32_item(4)	-- get viewport height
		-- position
		x := m_player.m_position_x
		y := m_player.m_position_y
		-- rendering the player pixel (dont remember how to make a filled circle could make a 3x3 pixel rect tho)
		a_renderer.draw_filled_rectangle((remap_vx(x, width) // m_automap_scale_factor -1),	-- we added 1 as offset for the dot being 3x3 with index starting at 1 so that it is centered.
							 			((remap_vy(y, height) // m_automap_scale_factor) - 101), 3, 3)
		create ray_vec.make_vec2 (0, 0)
		ray_length := 4
		ray_vec.set_x(x + ray_length)
		ray_vec.set_y(y + ray_length)
		--ray_vec.normalize
		--ray_vec.scale(ray_length)
		ray_vec.rotate(m_player.m_angle)
		--ray_x := remap_vx(((x + ray_length)*{DOUBLE_MATH}.cosine({ANGLE_UTILS}.to_rad(m_player.m_angle)).truncated_to_integer.to_integer_16),width)-- who tf is the retard who messed up this whole bit the values where all mismatched
		--ray_y := remap_vy(((y + ray_length)*{DOUBLE_MATH}.sine({ANGLE_UTILS}.to_rad(m_player.m_angle)).truncated_to_integer.to_integer_16), height)-- these were all fucked since forever ago
		a_renderer.draw_line (remap_vx(x, width) // m_automap_scale_factor,
							  (remap_vy(y, height) // m_automap_scale_factor)-100,
							  remap_vx(ray_vec.x.truncated_to_integer.to_integer_16, width) // m_automap_scale_factor,
							  (remap_vy(ray_vec.y.truncated_to_integer.to_integer_16, height) // m_automap_scale_factor) -100) -- long ass line
	end	-- end render_automap_player

	render_automap_node(a_renderer: GAME_RENDERER;a_node_id:INTEGER)
	local
		x,y,x2,y2: INTEGER_16
		width, height: INTEGER_32
		l_automap_color: GAME_COLOR
		l_node: NODE
	do
		l_node := m_nodes[a_node_id]
		-- set custom color/sdl args before the right node
		create l_automap_color.make (0, 255, 0, 255)	-- green
		a_renderer.set_drawing_color (l_automap_color)
		width  := a_renderer.viewport.integer_32_item(3)	-- get viewport width
		height := a_renderer.viewport.integer_32_item(4)	-- get viewport height
		-- position
		x := remap_vx(l_node.right_box_left, width)
		y := remap_vy(l_node.right_box_top, height)
		x2:= remap_vx(l_node.right_box_right, width) - remap_vx(l_node.right_box_left, width) + 1
		y2:= remap_vy(l_node.right_box_bottom, height) - remap_vy(l_node.right_box_top, height) + 1
		-- rendering the bounding box of the right node
		a_renderer.draw_rectangle(x,y-100,x2,y2+100)
		-- set custom color/sdl args before the left node
		create l_automap_color.make (255, 0, 0, 255)	-- red
		a_renderer.set_drawing_color (l_automap_color)
		-- position
		x := remap_vx(l_node.left_box_left, width)
		y := remap_vy(l_node.left_box_top, height)
		x2:= remap_vx(l_node.left_box_right, width) - remap_vx(l_node.left_box_left, width) + 1
		y2:= remap_vy(l_node.left_box_bottom, height) - remap_vy(l_node.left_box_top, height) + 1
		-- rendering the bounding box of the left node
		a_renderer.draw_rectangle(x,y-100,x2,y2+100)
	end	-- end render_automap_node

	render_automap_line(a_renderer: GAME_RENDERER;a_x1,a_y1,a_x2,a_y2:INTEGER_16)
	--local
	do
		-- renders a line on the viewport
		a_renderer.draw_line (a_x1, a_y1, a_x2, a_y2)-- draw_rectangle(x,y-100,x2,y2+100)
	end	-- end render_automap_line

	render_sub_sector(a_ssect_id:NATURAL_16)-- this might need to be integer_16 but i dont know that yet
	local
		l_ssect: SUBSECTOR
		l_seg: SEG
		start_vertex, end_vertex: POINT
        start_x, start_y, end_x, end_y, i: INTEGER_16
	do
	-- SEND TO VIEWPORT.E
	-- SEE BSP.PY FOR REFERENCE
		--l_ssect := m_ssects[a_ssect_id]
		--from
    	--	i := 0
		--until
    	--	i >= l_ssect.seg_count.as_integer_16
		--loop
		--	l_seg := m_segs[(l_ssect.first_seg_id.as_integer_16 + i)]
		--	start_vertex :=	m_vertexes[l_seg.start_vertex_id]
		--	end_vertex 	 :=	m_vertexes[l_seg.end_vertex_id]
--		add_wall_in_fov(start_vertex, end_vertex)
--
--			i := i + 1
--		end
		-- future
	end

	travel_bsp(a_index:NATURAL_16)
	local
		mask: NATURAL_16
	do
		mask := 0x8000
		if (a_index.bit_and(mask) = 0x8000) then	-- detect if we are a subsector
			render_sub_sector(a_index & (mask.bit_not)) --convert node to subsector
		else
			-- We arent a subsector lets see which leaf node side we are in
			if is_left_side(m_nodes[a_index]) then
				travel_bsp(m_nodes[a_index].left_child_id)
			else
				travel_bsp(m_nodes[a_index].right_child_id)
			end
		end
	end

	is_left_side(a_node: NODE): BOOLEAN
	local
		dx,dy: INTEGER_16
	do
		dx := m_player.m_position_x - a_node.x_partition
		dy := m_player.m_position_y - a_node.y_partition
		Result := (dx * a_node.change_y_partition - dy * a_node.change_x_partition) <= 0
	end

invariant
	valid_automap_scale: m_automap_scale_factor /= 0	-- dont want those nasty divisions by 0
end
