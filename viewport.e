note
	description: "Summary description for {VIEWPORT}."
	author: "Florent Perreault"
	date: "4/11/2024"
	revision: "1.0.0"

class
	VIEWPORT

create
	make
feature -- Attributes
    m_map: 			 MAP
	m_player:		 PLAYER
    m_renderer: 	 GAME_RENDERER
    m_render_x_size: INTEGER_16
    m_render_y_size: INTEGER_16
    m_automap_scale_factor: INTEGER_16

feature -- Initialization
    make(m_renderer: GAME_RENDERER;a_map: MAP; a_player: PLAYER)
        do
            m_renderer := m_renderer
			m_map := a_map
            m_player := a_player
        end

    init(a_map: MAP; a_player: PLAYER)
        do
            m_map := a_map
            m_player := a_player
        end
	update_map(a_map)
		do
			m_map := a_map
		ensure
			is_new_map: m_map = a_map
		end
feature -- Rendering

    render (is_automap: BOOLEAN)
        do
            if is_automap then
                render_auto_map
            else
                render_3d_view
            end
        end

	render_sub_sector(a_ssect_id:NATURAL_16)-- this might need to be integer_16 but i dont know that yet
	local
		l_ssect: SUBSECTOR
		l_seg: SEG
		--start_vertex, end_vertex: POINT
        start_x, start_y, end_x, end_y, i: INTEGER_16
	do
	-- SEND TO VIEWPORT.E
	-- SEE BSP.PY FOR REFERENCE
		l_ssect := m_map.m_ssects[a_ssect_id]
		from
    		i := 0
		until
    		i >= l_ssect.seg_count.as_integer_16
		loop
			l_seg := m_segs[(l_ssect.first_seg_id.as_integer_16 + i)]
			--start_vertex :=	m_vertexes[l_seg.start_vertex_id]
			--end_vertex   :=	m_vertexes[l_seg.end_vertex_id]
			--add_wall_in_fov(start_vertex, end_vertex)
			add_wall_in_fov(m_map.m_vertexes[l_seg.start_vertex_id],m_map.m_vertexes[l_seg.end_vertex_id])
			i := i + 1
		end
		-- future
	end

	add_vertices_to_fov(a_vert1,a_vert2:POINT)
		-- broken do not use yet
		local
			v1_angle,v2_angle,span,rw_angle: INTEGER_16
		do
			v1_angle := point_to_angle(a_vert1)
			v2_angle := point_to_angle(a_vert2)

			span := {ANGLE_UTILS}.normalize(v1_angle - v2_angle)
			if span < 180 then	-- if over or equal 180 deg, we cull the backface se we invert the `if` here to just handle the case of rendering
				-- FOV CALCULATIONS | SHIFT BY PLAYER ANGLE --
				rw_angle := v1_angle
				v1_angle := v1_angle - m_player.m_angle
				v2_angle := v2_angle - m_player.m_angle
				span := {ANGLE_UTILS}.normalize(v1_angle + (m_player.m_fov//2))
				if span > m_player.m_fov then -- cull extra if vertex is out of the FOV

				end

				if span > m_player.m_fov then

				end
			end
		end


    add_wall_in_fov (a_v1_angle, a_v2_angle: REAL_64)--a_seg: SEG;
		--takes the angles from viewpoint to the verticies locations in space and calculate where it would land in the viewport.
	    local
			v1x_screen, v2x_screen: INTEGER_16
		do
            v1x_screen = angle_to_screen(a_v1_angle)
            v2x_screen = angle_to_screen(a_v2_angle)

			-- draw a vertical line where vertexes would land X-wise in the viewport
			-- in other words we are rendering the edges of each wall within the player's FOV
			m_renderer.draw_line(v1x_screen, 0, v1x_screen, m_renderer.viewport.integer_32_item(4))
			m_renderer.draw_line(v2x_screen, 0, v2x_screen, m_renderer.viewport.integer_32_item(4))
        end

    init_frame
        do
            m_renderer.set_drawing_color (create {GAME_COLOR}.make (0, 0, 0, 0))
            m_renderer.clear
        end

    set_draw_color (r, g, b: NATURAL_8)
        do
            m_renderer.set_drawing_color(create{GAME_COLOR}.make(r, g, b, 255))
        end

    draw_rect (x1, y1, a_width, a_height: INTEGER)
        do
            -- Implementation
        end

    draw_line (x1, y1, x2, y2: INTEGER)
        do
            m_renderer.draw_line(x1,y1,x2,y2)
        end

feature {NONE} -- Internal rendering
    render_auto_map
		local
			l_xshift, l_yshift, i: INTEGER_16
			width, height: INTEGER_32
			v_end, v_start: POINT
			l_automap_color: GAME_COLOR
        do
            -- Implementation
			create l_automap_color.make (255, 255, 255, 255)	-- white
			m_renderer.set_drawing_color (l_automap_color)
			--create l_random.make
			width  := m_renderer.viewport.integer_32_item(3)	-- get viewport width
			height := m_renderer.viewport.integer_32_item(4)	-- get viewport height
			from
				i:= 1
			until
				i > (m_linedefs.count-1)
			loop
			m_renderer.set_drawing_color (l_automap_color)
			-- vertexes
			v_start := m_vertexes[m_linedefs[i.item].start_vertex.item+1]	-- also dont forget about the `+1` because of `eiffel moment`
			v_end 	:= m_vertexes[m_linedefs[i.item].end_vertex.item+1]		-- here as well

			-- rendering linedefs
			m_renderer.draw_line((remap_vx(v_start.x, width) // m_automap_scale_factor ),		-- remember to be careful around divisions that could have 0'd value
								 ((remap_vy(v_start.y, height) // m_automap_scale_factor) - 100),		-- cardinal to viewport manipulation
								 (remap_vx(v_end.x, width) // m_automap_scale_factor),
								 (remap_vy(v_end.y, height) // m_automap_scale_factor) - 100)
			i := i+1
			end	-- end loop
			render_automap_player
			render_automap_node_bounds(m_map.m_nodes.count)
        end

	-- MARK: automap_player
	render_automap_player
	local
		x,y,ray_x,ray_y: INTEGER_16
		--: REAL_64
		ray_vec: VEC2
		ray_length: INTEGER_16
		width, height: INTEGER_32
		l_automap_color: GAME_COLOR
	do
		-- set custom color/sdl args before the loop
		create l_automap_color.make (255, 0, 0, 255)	-- red
		m_renderer.set_drawing_color (l_automap_color)
		width  := m_renderer.viewport.integer_32_item(3)	-- get viewport width
		height := m_renderer.viewport.integer_32_item(4)	-- get viewport height
		-- position
		x := m_player.m_position_x
		y := m_player.m_position_y
		-- rendering the player pixel (dont remember how to make a filled circle could make a 3x3 pixel rect tho)
		m_renderer.draw_filled_rectangle((remap_vx(x, width) // m_automap_scale_factor -1),	-- we added 1 as offset for the dot being 3x3 with index starting at 1 so that it is centered.
							 			((remap_vy(y, height) // m_automap_scale_factor) - 101), 3, 3)
		create ray_vec.make_vec2 (0, 0)
		ray_length := 4
		ray_vec.set_x(x * ray_length)
		ray_vec.set_y(y * ray_length)
		ray_vec.rotate_degrees(m_player.m_angle)
		m_renderer.draw_line (remap_vx(x, width) // m_automap_scale_factor,
							  remap_vy(y, height) // m_automap_scale_factor-100,
							  remap_vx(ray_vec.x.truncated_to_integer.to_integer_16, width) // m_automap_scale_factor,
							  remap_vy(ray_vec.y.truncated_to_integer.to_integer_16, height) // m_automap_scale_factor -100) -- long ass line
	end	-- end render_automap_player

	--MARK: automap_node
	render_automap_node_bounds(a_node_id:INTEGER)
	local
		x,y,x2,y2: INTEGER_16
		width, height: INTEGER_32
		l_automap_color: GAME_COLOR
		l_node: NODE
	do
		l_node := m_nodes[a_node_id]
		-- set custom color/sdl args before the right node
		create l_automap_color.make (0, 255, 0, 255)	-- green
		m_renderer.set_drawing_color (l_automap_color)
		width  := m_renderer.viewport.integer_32_item(3)	-- get viewport width
		height := m_renderer.viewport.integer_32_item(4)	-- get viewport height
		-- position
		x := remap_vx(l_node.right_box_left, width)
		y := remap_vy(l_node.right_box_top, height)
		x2:= remap_vx(l_node.right_box_right, width) - remap_vx(l_node.right_box_left, width) + 1
		y2:= remap_vy(l_node.right_box_bottom, height) - remap_vy(l_node.right_box_top, height) + 1
		-- rendering the bounding box of the right node
		m_renderer.draw_rectangle(x,y-100,x2,y2+100)
		-- set custom color/sdl args before the left node
		create l_automap_color.make (255, 0, 0, 255)	-- red
		m_renderer.set_drawing_color (l_automap_color)
		-- position
		x := remap_vx(l_node.left_box_left, width)
		y := remap_vy(l_node.left_box_top, height)
		x2:= remap_vx(l_node.left_box_right, width) - remap_vx(l_node.left_box_left, width) + 1
		y2:= remap_vy(l_node.left_box_bottom, height) - remap_vy(l_node.left_box_top, height) + 1
		-- rendering the bounding box of the left node
		m_renderer.draw_rectangle(x,y-100,x2,y2+100)
	end	-- end render_automap_node_bounds


    render_3d_view
        do
            -- Implementation
        end

    angle_to_screen (a_angle: REAL_64): INTEGER_16
        local
			l_x,half_width: INTEGER_16
			l_angle: REAL_64
		do
            -- Implementation
			    l_x := 0;
				half_width := m_renderer.viewport.integer_32_item(3) // 2
    		-- Left side
    		if (a_angle > 90) then
    		    l_angle := a_angle - 90
    		    l_x := half_width - round({DOUBLE_MATH}.tangent(a_angle * PI / 180.0) * half_width)
    		else
    		-- Right side
    		    l_angle = 90 - a_angle
    		    l_x := round({DOUBLE_MATH}.tangent(a_angle * PI / 180.0) * half_width)
    		    l_x := l_x + half_width
			end

            Result := l_x -- Placeholder
        end

    remap_vx(a_vx:INTEGER_16;a_width:INTEGER_32): INTEGER
        -- remaps the given vertex `x` value to a viewport scaled version with padding as clamp
		local
			clamp_size,width_clamped:INTEGER_32
		do
			-- INIT VARS
			clamp_size := 30	-- clamp size in pixel (30 pixel from window border)
			width_clamped := a_width - clamp_size
			-- PREPARE YOURSELF FOR A MOUTHFUL
			result := ((m_xmin.max(a_vx.min(m_xmax)) - m_xmin) *
					  (width_clamped - clamp_size) // (m_xmax - m_xmin) + clamp_size).to_integer_16	-- xmax :D
		end

    remap_vy(a_vy:INTEGER_16;a_height:INTEGER_32): INTEGER
        local
			clamp_size,height_clamped:INTEGER_32
		do
			-- INIT VARS
			clamp_size := 30	-- clamp size in pixel (30 pixel from window border)
			height_clamped := a_height - clamp_size
			-- PREPARE YOURSELF FOR A MOUTHFUL
			result := (a_height - (m_ymin.max(a_vy.min(m_ymax)) - m_ymin) *
					  (height_clamped - clamp_size) // (m_ymax - m_ymin) - clamp_size).to_integer_16
		end
invariant
    renderer_not_void: m_renderer /= Void
end
