note
    description : "A game created in Eiffel."
    author      : ""
    generator   : "Eiffel Game2 Project Wizard"
    date        : "2024-01-19 01:55:16.312 +0000"
    revision    : "0.1"

class
    APPLICATION

inherit
	CONSOLE_LOGGER
		rename
			default_create as make_logger
		end
	GAME_LIBRARY_SHARED		-- To use `game_library'
		Select
			default_create
		end
	IMG_LIBRARY_SHARED		-- To use `image_file_library'
	AUDIO_LIBRARY_SHARED	-- To use `audio_library`
	TEXT_LIBRARY_SHARED		-- To use `text_library`

create
    make

feature {NONE} -- Initialization

    make
            -- Running the game.
        local
        	loader: WAD_LOADER
        	l_engine: ENGINE
        	is_error: BOOLEAN
        do
            -------------------
            -- Try to read a wad
            -- located in `FLDoom/WADs`
            -------------------
            log_info("===== FLDOOM VERSION 0.0.6alpha =====")
            create loader.make ("WADs/doom1.wad") --("WADs/test.bin")--("WADs/DOOM.WAD")
            is_error := not loader.load_wad
			if is_error then
				log_err_crit("WAD loading unsuccessful, see logs for details.")
			end
			game_library.enable_video -- Enable the video functionalities
            audio_library.enable_playback	-- Permit to use the Audio functionnality
            text_library.enable_text
    	    image_file_library.enable_image (true, false, false)  -- Enable PNG image (but not TIF or JPG).
	    	create l_engine.make_with_maps(loader.map_list)
	    	if not l_engine.has_error and not is_error then
  	    		l_engine.run
	    	end

        end

end
