# FLDoom
[![EIFFEL](https://img.shields.io/badge/Created_Using-Eiffel_Studio-blue?style=plastic)](https://www.eiffel.com/) [![SDL](https://img.shields.io/badge/Graphical_Solution-SDL_2-_?style=flat-square&color=173556)](https://www.libsdl.org) [![OS - Linux](https://img.shields.io/badge/OS-Linux-blue?style=plastic&logo=linux&logoColor=white)](https://www.linux.org/ "Go to Linux homepage") [![OS - Windows](https://img.shields.io/badge/OS-Windows-blue?style=plastic&logo=windows&logoColor=white&color=004fe1)](https://www.microsoft.com/ "Go to Microsoft homepage")

## Description
FLDoom is a DOOM engine "PORT" of the original [ULTIMATE DOOM and DOOM II engine written in C/C++](https://github.com/id-Software/DOOM), using the EIFFEL programming language. this will most probably end once we reach a point where the user can actively walk around the maps loaded from a DOOM-style WAD file. if i have the time and energy for it i might consider doing actual recreation of doom, however this is far fetched considering the current level of proficiency that i have with the language itself.

## Roadmap
- ~~Fix the issue making it that it takes 6120 seconds(1h42m) to load a 12MB wad file.~~
    - ~~Fix directory/lump read code because it doesnt read the correct amount of data size for some(yeah idk why only some works and some dont) given lump.~~
    - use a form of MEMCPY for byte reading as well because iteration is wasted cpu ressources (and time consuming)
- Display 2D Sectors read from a WAD file.
- Display the 128x GRID blockmap for Subsectors & edge detection.
- Implement a working BSP Resolver
- Render 3D Walls from sectors loaded from the WAD's BSP maps (maybe add support for UDMF map format too)

## Breakthroughs or Near future Code ideas
- Try to fix the issue of file reading time by using eiffel's version of c/c++'s `STD::MEMCPY()` using pointer references: [see source code](https://www.eiffel.org/files/doc/static/20.05/libraries/base/pointer_ref_flatshort.html#f_memory_copy)

# Compilation Guide / Guide de compilation
- Francais:

1. **Clonez ce repo et téléchargez [EiffelStudio 19.05](https://www.eiffel.com/eiffelstudio/)**
2. **Installer la librairie [EiffelGame2](https://github.com/tioui/Eiffel_Game2) dans votre repertoire Eiffel**
3. **Prenez les DLLs fournis dans les examples et copier/coller les dans le repertoire du projet**
4. **Une fois cela fait, ouvrez le fichier *.ecf* et compilez le projet**

- English:

1. **Clone this repo and download [EiffelStudio 19.05](https://www.eiffel.com/eiffelstudio/)**
2. **Install the [EiffelGame2](https://github.com/tioui/Eiffel_Game2) game librairy for Eiffel and install it to your local Eiffel folder**
3. **Take the DLLs found within the examples and copy/paste them to the FLDoom root folder**
4. **Once thats done open the *.ecf* file and compile the project**


## Authors
- [**Florent Perreault**](https://gitlab.com/MapelSiroup/) - Main programmer & Maintainer

## Acknowledgment
- [**Louis Marchand**](https://github.com/tioui)    - my OOP teacher which helps with bits of code here and there.

- [**Gachoud Philippe**](https://github.com/phgachoud)  - Console ANSI character escape reader utility in eiffel for Windows' Terminal/Console
    - see: [`console_ansi_control.e`](https://gitlab.com/MapelSiroup/fldoom/-/blob/main/CONSOLE_Utils/console_ansi_control.e?ref_type=heads)
- [**idSoftware**](https://github.com/id-Software/)  - for Doom

## License
-  based on original Linux Doom as published by "id Software"
-  Copyright (C) 1993-1996 by id Software, Inc.

**For the License, consult the `LICENSE` file.**
**For file specific or code snippet specific license/rights attribution, refer to source code file.**
