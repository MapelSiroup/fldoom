note
	description: "Summary description for {E_MAP_LUMPS_INDEX}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

expanded class
	E_MAP_LUMPS_INDEX
feature -- VALUE
	value: NATURAL_16
feature -- TYPES
	-- normally the `value`(indexes in this case) would start at one but for the sake of ease of use and less retarded code i gave +1 to each index

	MAPMARKER: NATURAL_16	= 1	-- not fully necessary because first entry is always a 0'd value simply indicating the start of a map's lump_data at the offset for the map in question
    THINGS: NATURAL_16 		= 2
    LINEDEFS: NATURAL_16	= 3
    SIDEDDEFS: NATURAL_16	= 4
    VERTEXES: NATURAL_16	= 5
    SEGS: NATURAL_16		= 6
    SSECTORS: NATURAL_16	= 7
    NODES: NATURAL_16		= 8
    SECTORS: NATURAL_16		= 9
    REJECT: NATURAL_16		= 10
    BLOCKMAP: NATURAL_16	= 11
    NEXT_MAP: NATURAL_16	= 12	-- is this necessary ? i have read about it somewhere but cant find it anymore does not appear anywhere else? keeping it just because.
invariant
	value = MAPMARKER or
    value = THINGS or
    value = LINEDEFS or
    value = SIDEDDEFS or
    value = VERTEXES or
    value = SEGS or
    value = SSECTORS or
    value = NODES or
    value = SECTORS or
    value = REJECT or
    value = NEXT_MAP
end
