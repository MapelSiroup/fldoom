note
	description: "ENUM CLASS for linedefs flag values {E_LINEDEF_FLAGS}"
	author: "Florent Perreault"
	date: "1/26/2024"
	revision: "1.0.0"

expanded class
	E_LINEDEF_FLAGS
feature -- VALUE
	value: NATURAL_16
feature -- TYPES
	BLOCKING: NATURAL_16 		= 0
    BLOCKMONSTERS: NATURAL_16 	= 1
    TWOSIDED: NATURAL_16	    = 2
    DONTPEGTOP: NATURAL_16		= 4
    DONTPEGBOTTOM: NATURAL_16	= 8
    SECRET: NATURAL_16 			= 16
    SOUNDBLOCK: NATURAL_16		= 32
    DONTDRAW: NATURAL_16		= 64
    DRAW: NATURAL_16 			= 128
invariant
	value = BLOCKING or
    value = BLOCKMONSTERS or
    value = TWOSIDED or
    value = DONTPEGTOP or 
    value = DONTPEGBOTTOM or 
    value = SECRET or 
    value = SOUNDBLOCK or 
    value = DONTDRAW or 
    value = DRAW
end
